import UIKit

class GetPhoneNumberVC: UIViewController {
    
    var randNum : String!
    var phoneNum : String!
    
    let imageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SMS")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let textView : UITextView = {
        let tv = UITextView()
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "getPhoneNumber", comment: "")
//        guard let customFont = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        tv.font = UIFontMetrics(forTextStyle: .title3).scaledFont(for: customFont)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tv.adjustsFontForContentSizeCategory = true
        tv.isEditable = false
        tv.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let textField : UITextField = {
        let tf = UITextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNumberPlaceHolder", comment: "")
        tf.textAlignment = .center
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        tf.keyboardType = .numberPad
//        guard let customFont = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        tf.font = UIFontMetrics(forTextStyle: .caption2).scaledFont(for: customFont)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let button : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirmPhoneNumber", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.textAlignment = .center
//        guard let customFont = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        btn.titleLabel?.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: customFont)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        btn.titleLabel?.adjustsFontForContentSizeCategory = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(submitAct), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupImageViewLayout()
        setupTextViewLayout()
        setupTextFieldLayout()
        setupButtonLayout()
        textField.delegate = self
    }
    
    fileprivate func setupImageViewLayout() {
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor,constant: view.frame.height/6).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: view.frame.width/1.6).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: view.frame.width/1.6).isActive = true
    }
    
    fileprivate func setupTextViewLayout() {
        view.addSubview(textView)
        textView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16).isActive = true
        textView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    fileprivate func setupTextFieldLayout(){
        view.addSubview(textField)
        let heightSize = UIFont.labelFontSize + 30
        textField.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 16).isActive = true
        textField.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 16).isActive = true
        textField.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -16).isActive = true
        textField.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        textField.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupButtonLayout(){
        view.addSubview(button)
        let heightSize = UIFont.labelFontSize + 30
        button.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 16).isActive = true
        button.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: view.frame.width/6).isActive = true
        button.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -view.frame.width/5).isActive = true
        button.heightAnchor.constraint(equalToConstant: heightSize).isActive = true

        button.setCornerRadius(size: heightSize / 2)
    }
    
    func genRandomNumber() -> String{
        let rand = Int.random(in: 1000..<10000)
        print(rand)
        return String(rand)
    }

    @objc func submitAct(sender : UIButton) {
        if validatePhoneNumber(value: textField.text!) {
            randNum = genRandomNumber()
            phoneNum = textField.text
            print(phoneNum!)
            sendData()
            goToVerificationPage()
        } else {
            showError()
        }
    }
    
    func showError() {
        let error = UIAlertController(title: "Error", message: "This phone number isn't correct", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        self.present(error, animated: true)
    }
    
    func sendData(){
        let url = URL(string: "https://hamrajoo.net/test/send.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters = "mobile=\(String(describing: phoneNum!))&code=\(String(describing: randNum!))"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            //Let's convert response sent from a server side script to a NSDictionary object:
            print("data : \(data)")
//            if let data = data {
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
//                    print("json : \(json)")
//                } catch {
//                    print(error)
//                }
//            }
            
            
        }
        task.resume()
    }
    
    func validatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(0))[0-9]{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func goToVerificationPage(){
        let verification = verificationVC()
        //let sb = UIStoryboard(name: "Main", bundle: nil)
        //let verificationVC = sb.instantiateViewController(withIdentifier: "verifySMS") as! verificationVC
        verification.validationNumber = randNum
        verification.phoneNumber = phoneNum
        verification.modalTransitionStyle = .flipHorizontal
        self.present(verification, animated: true)
    }

}

extension GetPhoneNumberVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
