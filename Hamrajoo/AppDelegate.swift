import UIKit
import GooglePlaces
import GoogleMaps
import Firebase

var myDeviceToken = ""

struct defaultsKeys {
    static let statusKey = "status"
    static let userIdKey = "user_id"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSPlacesClient.provideAPIKey("AIzaSyA1rr_O6IcDgyq_-uuYU3VVivju7yLPqPg")
        GMSServices.provideAPIKey("AIzaSyA1rr_O6IcDgyq_-uuYU3VVivju7yLPqPg")
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        //window?.rootViewController = ViewController()
        
        //window?.rootViewController = IntroSliderVC()
        
        //let layout = UICollectionViewFlowLayout()
        //layout.scrollDirection = .horizontal
        //window?.rootViewController = SwipingController(collectionViewLayout: layout)
        
        //window?.rootViewController = SelectLanguageVC()
        
        FirebaseApp.configure()
        
//        let launchvc = LaunchScreenVC()
//        launchvc.modalPresentationStyle = .fullScreen
//        window?.rootViewController = launchvc
        
//        let signUpVC = SignUpVC()
//        let navCnt = UINavigationController(rootViewController: signUpVC)
//        navCnt.navigationBar.isTranslucent = false
//        window?.rootViewController = navCnt
        
        //window?.rootViewController = MainPageVC()
        
//        let mainPage = MainPageVC()
//        let navCnt = UINavigationController(rootViewController: mainPage)
//        navCnt.navigationBar.isTranslucent = false
//        window?.rootViewController = navCnt
        
          //window?.rootViewController = ContainerController()
        
        let driverSignUp = DriverSignUpVC()
        let navCnt = UINavigationController(rootViewController: driverSignUp)
        window?.rootViewController = navCnt
        
//        window?.rootViewController = MapsVC()
        
//        window?.rootViewController = MapTestVC()
        
//        window?.rootViewController = ShowNotConnectedVC()
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().fcmToken
        myDeviceToken = Messaging.messaging().fcmToken!
    }
    
    class func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

