import UIKit

class IntroSliderVC: UIViewController {
    
    private let topImageContainer : UIView = {
        let cn = UIView()
        cn.backgroundColor = .blue
        cn.translatesAutoresizingMaskIntoConstraints = false
        return cn
    }()
    
    private let introImage : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "carpooling")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    private let titleText : UITextView = {
        let tv = UITextView()
//        tv.text = "Carpooling"
//        guard let customFont = UIFont(name: "ComicSansMS", size: 30) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        tv.font = UIFontMetrics(forTextStyle: .largeTitle).scaledFont(for: customFont)
//        tv.adjustsFontForContentSizeCategory = true
//        let attributeString = NSMutableAttributedString(string: "Hello", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 80)])
//        tv.attributedText = attributeString
    
        //tv.font = UIFont(name: "ComicSansMS", size: 30)
        tv.isEditable = false
        tv.textColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    private let discText : UITextView = {
        let tv = UITextView()
        tv.text = "Travel together and share your car"
        guard let customFont = UIFont(name: "ComicSansMS", size: 20) else {
            fatalError("""
        Failed to load the "CustomFont-Light" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """
            )
        }
        tv.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: customFont)
        tv.adjustsFontForContentSizeCategory = true
        tv.isEditable = false
        tv.textColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        tv.backgroundColor = .purple
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let prevButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("PREV", for: .normal)
        btn.backgroundColor = .black
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let nextButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("NEXT", for: .normal)
        btn.backgroundColor = .brown
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let pageControl : UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.currentPage = 0
        pc.numberOfPages = 5
        pc.currentPageIndicatorTintColor = .white
        pc.pageIndicatorTintColor = .gray
        return pc
    }()
    
    fileprivate func setupImageLayout(){
        view.addSubview(topImageContainer)
        topImageContainer.addSubview(introImage)
        topImageContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        topImageContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topImageContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topImageContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        introImage.centerXAnchor.constraint(equalTo: topImageContainer.centerXAnchor).isActive = true
        introImage.centerYAnchor.constraint(equalTo: topImageContainer.centerYAnchor).isActive = true
        introImage.heightAnchor.constraint(equalTo: topImageContainer.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    fileprivate func setupTitleTextViewLayout(){
        view.addSubview(titleText)
        titleText.topAnchor.constraint(equalTo: topImageContainer.bottomAnchor).isActive = true
        titleText.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        titleText.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        //titleText.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: 0).isActive = true
        titleText.heightAnchor.constraint(equalToConstant: 70).isActive = true
    }
    
    fileprivate func setupDiscTextViewLayout(){
        view.addSubview(discText)
        discText.topAnchor.constraint(equalTo: titleText.bottomAnchor).isActive = true
        discText.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 24).isActive = true
        discText.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -24).isActive = true
        discText.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        //discText.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    fileprivate func setupBottomStackViewLayout(){
//        view.addSubview(bottomStackView)
//        bottomStackView.addSubview([prevButton,nextButton])
//        bottomStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//        bottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//        bottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//        bottomStackView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.2).isActive = true
        
        let stack = UIStackView(arrangedSubviews: [prevButton,pageControl,nextButton])
        stack.backgroundColor = .gray
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .fillEqually
        view.addSubview(stack)
        
        stack.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.05).isActive = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        setupImageLayout()
        setupTitleTextViewLayout()
        setupDiscTextViewLayout()
        setupBottomStackViewLayout()
    }

}
