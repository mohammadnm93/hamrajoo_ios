import UIKit
import Firebase

class LaunchScreenVC: UIViewController {

    let enText : UITextView = {
        let tv = UITextView()
        tv.text = "HAMRAJOO"
        tv.font = UIFont(name: "ComicSansMS-Bold", size: 50)
        tv.isEditable = false
        tv.textColor = .white
        tv.backgroundColor = .clear
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let faText : UITextView = {
        let tv = UITextView()
        tv.text = "سامانه هوشمند همراجو"
        tv.font = UIFont(name: "IRANSansWeb", size: 30)
        tv.isEditable = false
        tv.textColor = .white
        tv.backgroundColor = .clear
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setGradientBackground()
        setupTitleTextViewLayout()
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(splashTimeOut(sender:)), userInfo: nil, repeats: false)
    }
    
    @objc func splashTimeOut(sender : Timer){
        let selectLanguage = SelectLanguageVC()
        selectLanguage.modalTransitionStyle = .flipHorizontal
        present(selectLanguage, animated: true)
    }
    
    func setupTitleTextViewLayout(){
        let stack = UIStackView(arrangedSubviews: [enText,faText])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.setCustomSpacing(10, after: enText)
        view.addSubview(stack)
        
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 8).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -8).isActive = true
        stack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        stack.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor,multiplier: 0.3).isActive = true
    }
    
}
