import Foundation
import UIKit

extension UIView {
    func setGradientBackground() {
        let colorBottom =  UIColor(red: 16.0/255.0, green: 84.0/255.0, blue: 158.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 20.0/255.0, green: 146.0/255.0, blue: 143.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at:0)
    }
    
    func showAllFontNames(){
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
    }
    
    func setCornerRadius(size: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = size
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

extension UICollectionViewFlowLayout {
    
    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return true
    }
}

class LeftPaddedTextField : UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            return CGRect(x: bounds.origin.x - 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            return CGRect(x: bounds.origin.x + 15, y: bounds.origin.y, width: bounds.width + 15, height: bounds.height)
        }
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            return CGRect(x: bounds.origin.x - 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            return CGRect(x: bounds.origin.x + 15, y: bounds.origin.y, width: bounds.width + 15, height: bounds.height)
        }
    }
}

