import UIKit
import GoogleMaps
import GooglePlaces

class MapTestVC: UIViewController {
    
    var locationManager = CLLocationManager()
    var currentLocation : CLLocation?
    var mapView : GMSMapView!
    var camera : GMSCameraPosition!
    var marker : GMSMarker!
    var secondMarker : GMSMarker!
    let zoomLevel : Float = 17.0
    let defaultLocation = CLLocation(latitude: 35.715298, longitude: 51.404343)
    let geocoder = GMSGeocoder()
    let secondGeocoder = GMSGeocoder()
    var didSourceSelected : Bool = false
    var didDestinationSelected : Bool = false
    var fromTxt : String = ""
    var toTxt : String = ""
    var timer = Timer()
    
    let fakeMarker : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "marker")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = .clear
        return iv
    }()
    
    fileprivate func setupFakeMarker(){
        view.addSubview(fakeMarker)
        fakeMarker.centerXAnchor.constraint(equalTo: mapView.centerXAnchor).isActive = true
        fakeMarker.centerYAnchor.constraint(equalTo: mapView.centerYAnchor,constant: -10).isActive = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        fakeMarker.isUserInteractionEnabled = true
        fakeMarker.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        if !didSourceSelected {
            marker = GMSMarker(position: camera.target)
            marker.icon = UIImage(named: "marker")
            marker.isDraggable = true
            marker.isTappable = true
            marker.map = mapView
            print(marker.position)
            mapView.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: camera.target.latitude + 0.001, longitude: camera.target.longitude), zoom: 17))
            
            didSourceSelected = true
            didDestinationSelected = false
            backButton.isHidden = false
            geocoder.reverseGeocodeCoordinate(marker.position) { (response, error) in
                guard error == nil else {
                    return
                }
                if let result = response?.firstResult() {
                    self.fromTxt = result.lines?[0] ?? "no value f"
                    print(self.fromTxt)
                }
            }
            
        } else if !didDestinationSelected {
            secondMarker = GMSMarker(position: camera.target)
            secondMarker.icon = UIImage(named: "marker")
            secondMarker.isDraggable = true
            secondMarker.isTappable = true
            secondMarker.map = mapView
            print(secondMarker.position)
            didDestinationSelected = true
            mapView.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: (marker.position.latitude + secondMarker.position.latitude)/2, longitude: (marker.position.longitude + secondMarker.position.longitude)/2), zoom: 10))
            fakeMarker.isHidden = true
            DispatchQueue.main.async {
                self.secondGeocoder.reverseGeocodeCoordinate(self.secondMarker.position) { (response, error) in
                    guard error == nil else {
                        return
                    }
                    if let result = response?.firstResult() {
                        self.toTxt = result.lines?[0] ?? "no value t"
                        print(self.toTxt)
                        let detailVC = PassengerTripDetailVC()
                        detailVC.modalPresentationStyle = .overCurrentContext
                        detailVC.fromText = self.fromTxt
                        detailVC.toText = self.toTxt
                        detailVC.delegate = self
                        self.present(detailVC, animated: true)
                    }
                }

            }
        }
        
    }
    
    @objc func autocompleteClicked(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    let searchMapsBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.setTitle("Search", for: .normal)
        btn.setTitleColor(.gray, for: .normal)
        btn.setTitleColor(.black, for: .highlighted)
        btn.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func makeSearchAndBackButton() {
        view.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.07).isActive = true
        containerView.addSubview(backButton)
        backButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 4).isActive = true
        backButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 4).isActive = true
        backButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,constant: -4).isActive = true
        backButton.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.1).isActive = true
        
        containerView.addSubview(searchMapsBtn)
        searchMapsBtn.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        searchMapsBtn.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        searchMapsBtn.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        searchMapsBtn.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.9).isActive = true
        containerView.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceLeftToRight
        
    }
    
    let backButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.setImage(UIImage(named: "back"), for: .normal)
        btn.isHidden = false
        btn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func backAction() {
        if !didSourceSelected && !didDestinationSelected {
            self.dismiss(animated: true, completion: nil)
        } else if didSourceSelected {
            if secondMarker != nil {
                secondMarker.map = nil
            }
            marker.map = nil
            didSourceSelected = false
            didDestinationSelected = false
            mapView.animate(toLocation: marker.position)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLocationManager()
        initMapView()
        makeSearchAndBackButton()
        setupFakeMarker()
        //nightMode()
    }
    
    func nightMode(){
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        //    override var preferredStatusBarStyle: UIStatusBarStyle {
        //        return .lightContent
        //    }
    }
    
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    func initMapView(){
        camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude, longitude: defaultLocation.coordinate.longitude, zoom: zoomLevel)
        mapView = GMSMapView(frame: view.safeAreaLayoutGuide.layoutFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.isHidden = true
        //mapView.isTrafficEnabled = true
        mapView.delegate = self
        view.addSubview(mapView)
    }
    
}

extension MapTestVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Place ID: \(String(describing: place.placeID))")
        print("Place attributions: \(String(describing: place.coordinate))")
        
        dismiss(animated: false, completion: nil)
        //mapView.animate(toLocation: place.coordinate)
        DispatchQueue.main.async {
            self.mapView.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: place.coordinate.latitude,longitude: place.coordinate.longitude), zoom: 16))
        }
        
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension MapTestVC : didDismissViewController {
    func dismissVC() {
        fakeMarker.isHidden = false
        secondMarker.map = nil
        didDestinationSelected = false
        mapView.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: secondMarker.position.latitude, longitude:secondMarker.position.longitude), zoom: 17))
    }
}

extension MapTestVC : GMSMapViewDelegate {
    
    //not happend yet
    func mapView(_ mapView: GMSMapView, didTapMyLocation location: CLLocationCoordinate2D) {
        print("did tap my location")
    }
    //no need yet
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        //print("lat : \(coordinate.latitude)\nlong : \(coordinate.longitude)")
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("will move")
        //mapView.clear()
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("did drag marker")
        
    }
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        print("did tap")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("did end dragging")
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap marker")
        return false
    }
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("did begin dragging")
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idle at")
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("did tap info window of")
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("did change")
        camera = GMSCameraPosition.camera(withLatitude: position.target.latitude,
                                          longitude: position.target.longitude,
                                          zoom: zoomLevel)
    }
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        print("did close info window of")
    }
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("did long press info window of")
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("did tap at")
    }
    
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D) {
        print("did tap POI with PlaceID")
    }
}

extension MapTestVC : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                          longitude: location.coordinate.longitude,
                                          zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.animate(to: camera)
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
}
