import UIKit

protocol didDismissViewController {
    func dismissVC()
}

class PassengerTripDetailVC: UIViewController {
    
    var bottomViewStack : UIStackView!
    var delegate : didDismissViewController?
    
    var fromText : String = ""
    var toText : String = ""
    var dateText : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        view.addGestureRecognizer(tap)
        setupContainerViewLayout()
        setupDetailButtonsLayout()
        setupDatePickerLayout()
    }
    
    @objc func dismissVC() {
        delegate?.dismissVC()
        dismiss(animated: true, completion: nil)
    }
    
    let bottomContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 30
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    let dateBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "blue")
        btn.titleLabel?.textAlignment = .center
        btn.setCornerRadius(size: 20)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "passengerDetail_dateBtn", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
        }
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(dateButtonClicked), for: .touchUpInside)
        return btn
    }()
    
    @objc func dateButtonClicked(){
        datePickerContainerView.isHidden = false
    }
    
    let searchBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.titleLabel?.textAlignment = .center
        btn.setCornerRadius(size: 20)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "passengerDetail_searchBtn", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
        }
        btn.tintColor = .white
        btn.alpha = 0.5
        btn.isEnabled = false
        btn.addTarget(self, action: #selector(didSearchTapped), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func didSearchTapped(){
        let tripDialog = TripDetailDialog()
        tripDialog.fromText = self.fromText
        tripDialog.toText = self.toText
        tripDialog.date = self.dateText
        present(tripDialog, animated: true)
    }
    
    let datePickerContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .green
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setCornerRadius(size: 20)
        view.isHidden = true
        return view
    }()
    
    let datePicker : UIDatePicker = {
        let dp = UIDatePicker()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            dp.timeZone = TimeZone(identifier: "IRST")
            dp.calendar = Calendar(identifier: .persian)
            dp.locale = Locale(identifier: "fa_IR")
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            dp.timeZone = TimeZone(identifier: "EST")
            dp.locale = Locale(identifier: "en")
        }
        dp.datePickerMode = .date
        dp.backgroundColor = .white
        dp.isHidden = false
        dp.translatesAutoresizingMaskIntoConstraints = false
        return dp
    }()
    
    let selectButton : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor(named: "red")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "passengerSelectBtn", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
        }
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.textAlignment = .center
        btn.addTarget(self, action: #selector(didSelectTapped), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func didSelectTapped(){
        let date = datePicker.date
        let formatter = DateFormatter()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            formatter.calendar = Calendar(identifier: .persian)
            formatter.dateFormat = "yyyy/MM/dd"
            dateText = formatter.string(from: date)
            datePickerContainerView.isHidden = true
            searchBtn.isEnabled = true
            searchBtn.alpha = 1.0
            dateBtn.setTitle(formatter.string(from: date), for: .normal)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            formatter.dateFormat = "yyyy/MM/dd"
            dateText = formatter.string(from: date)
            datePickerContainerView.isHidden = true
            searchBtn.isEnabled = true
            searchBtn.alpha = 1.0
            dateBtn.setTitle(formatter.string(from: date), for: .normal)
        }
        
    }
    
    func convertEngNumToPersianNum(num: String)->String{
        print(num)
        let number = NSNumber(value: Int(num)!)
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let faNumber = format.string(from: number)
        return faNumber!
    }
    
    fileprivate func setupContainerViewLayout(){
        view.addSubview(bottomContainerView)
        bottomContainerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        bottomContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 2).isActive = true
        bottomContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -2).isActive = true
        bottomContainerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.09).isActive = true
    }
    
    fileprivate func setupDetailButtonsLayout(){
        bottomViewStack = UIStackView(arrangedSubviews: [dateBtn, searchBtn])
        bottomViewStack.axis = .horizontal
        bottomViewStack.distribution = .fillEqually
        bottomViewStack.backgroundColor = .gray
        bottomViewStack.customSpacing(after: dateBtn)
        bottomViewStack.spacing = 16
        bottomViewStack.translatesAutoresizingMaskIntoConstraints = false
        bottomContainerView.addSubview(bottomViewStack)
        
        bottomViewStack.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 16).isActive = true
        bottomViewStack.trailingAnchor.constraint(equalTo: bottomContainerView.trailingAnchor, constant: -16).isActive = true
        bottomViewStack.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 10).isActive = true
        bottomViewStack.bottomAnchor.constraint(equalTo: bottomContainerView.bottomAnchor, constant: -10).isActive = true
        //stack.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    fileprivate func setupDatePickerLayout(){
        view.addSubview(datePickerContainerView)
        datePickerContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        datePickerContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        datePickerContainerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor,constant: -32).isActive = true
        datePickerContainerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.5).isActive = true
        
        datePickerContainerView.addSubview(datePicker)
        datePicker.topAnchor.constraint(equalTo: datePickerContainerView.topAnchor).isActive = true
        datePicker.leadingAnchor.constraint(equalTo: datePickerContainerView.leadingAnchor).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: datePickerContainerView.trailingAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalTo: datePickerContainerView.heightAnchor, multiplier: 0.8).isActive = true
        
        datePickerContainerView.addSubview(selectButton)
        selectButton.topAnchor.constraint(equalTo: datePicker.bottomAnchor).isActive = true
        selectButton.leadingAnchor.constraint(equalTo: datePicker.leadingAnchor).isActive = true
        selectButton.trailingAnchor.constraint(equalTo: datePicker.trailingAnchor).isActive = true
        selectButton.bottomAnchor.constraint(equalTo: datePickerContainerView.bottomAnchor).isActive = true
        
        
    }
}

class TripDetailDialog : UIViewController {
    
    var stack : UIStackView!
    
    var fromText : String = ""
    var toText : String = ""
    var date : String = ""
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
        setupLayouts()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 20)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var fromTextView : UITextView = {
        let tv = UITextView()
        tv.textColor = .black
        tv.text = "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "detailFrom", comment: "")) : \(self.fromText)"
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .left
        }
        tv.isEditable = false
        tv.isSelectable = false
        tv.isScrollEnabled = true
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    lazy var toTextView : UITextView = {
        let tv = UITextView()
        tv.textColor = .black
        tv.text = "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "detailTo", comment: "")) : \(self.toText)"
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .left
        }
        tv.isEditable = false
        tv.isSelectable = false
        tv.isScrollEnabled = true
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    lazy var dateTextView : UITextView = {
        let tv = UITextView()
        tv.textColor = .black
        tv.isEditable = false
        tv.text = "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "detailDate", comment: "")) : \(self.date)"
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
            tv.textAlignment = .left
        }
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let searchBtn : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor(named: "red")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "detailSearch", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
        }
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate func setupLayouts() {
        view.addSubview(containerView)
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 16).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: -20).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.4).isActive = true
        
        stack = UIStackView(arrangedSubviews: [fromTextView,toTextView,dateTextView])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.backgroundColor = .gray
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(stack)
        stack.topAnchor.constraint(equalTo: containerView.topAnchor,constant: 16).isActive = true
        stack.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 8).isActive = true
        stack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -8).isActive = true
        stack.heightAnchor.constraint(equalTo: containerView.heightAnchor,multiplier: 0.7).isActive = true
        
        containerView.addSubview(searchBtn)
        searchBtn.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        searchBtn.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: stack.bottomAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
    
}
