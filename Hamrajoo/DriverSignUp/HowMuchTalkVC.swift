import UIKit
import RadioGroup

class HowMuchTalkVC: UIViewController {
    
    var delegate : DismissHowMuchYouTalk?
    var radioGroup : RadioGroup!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewsLayout()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        view.addGestureRecognizer(tap)
        configureRadioGroup()
    }
    
    @objc fileprivate func dismissVC () {
        delegate?.didDismissHowMuchYouTalkVC()
        self.dismiss(animated: true, completion: nil)
    }

    fileprivate func configureRadioGroup(){
        radioGroup = RadioGroup(titles: ["I do not talk a lot" , "I talk a little bit", "I like talking during my trip"])
        radioGroup.addTarget(self, action: #selector(optionSelected), for: .valueChanged)
        radioGroup.isButtonAfterTitle = true
        radioGroup.tintColor = UIColor(named: "green")
        radioGroup.selectedColor = UIColor(named: "red")
        radioGroup.titleColor = UIColor(named: "darkBlue")
        radioGroup.buttonSize = 30
        radioGroup.titleFont = UIFont(name: "ComicSansMS", size: 20)
        
        radioGroup.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(radioGroup)
        radioGroup.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        radioGroup.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        radioGroup.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -12).isActive = true
        radioGroup.topAnchor.constraint(equalTo: titleLb.bottomAnchor, constant: 12).isActive = true
    }
    
    @objc fileprivate func optionSelected(){
        delegate?.didRadioGroupSelected(index: radioGroup.selectedIndex)
        dismissVC()
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleLb : UILabel = {
        let lb = UILabel()
        lb.text = "How much you talk?!"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 22)
        lb.textAlignment = .center
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    fileprivate func setupViewsLayout() {
        view.addSubview(containerView)
        containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -2).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
      
        containerView.addSubview(titleLb)
        titleLb.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8).isActive = true
        titleLb.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        titleLb.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        
    }

}
