import UIKit

struct ExpandableNames {
    var isExpanded = false
    var names : [String]
    var carNamesId : [String]
    var brandId : [String]
}

class AddCarVC: UIViewController {
    
    var didChooseCarCardFrontImage = false
    var didChooseCarCardBehindImage = false
    var showChoosImageVC : UIViewController!
    var showCarFacilitiesVC : UIViewController!
    var delegate : DismissAddCarVC?
    var stack : UIStackView!
    let licenceData = ["ه" ,"و" ,"ن" ,"م" ,"گ" ,"ق" ,"ع" ,"ط" ,"ص" ,"س" ,"د" ,"چ-ژ" ,"ج" ,"ت" ,"ب" ,"الف","ی","D" ,"S"]
    //var colorList = [String]()
    var carList = [[String : String]]()
    var brand = [String]()
    var colorsList = [colorsNameWithID]()
    
    var myTableView : UITableView!
    var cellId = "cellId"
    var expandableArray = [ExpandableNames]()
    var carFacilitiesData = [SwitchStatusModel]()
    var carModelsListWithId = [carModelsWithId]()
    var dataWillSend = [[String : Any]]()
    var selectedLetter = ""
    var brandPlus = ""
    var brandID = ""
    var carID = ""
    var colorID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hiddeLists(_:)))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
        setupViewsLayout()
        configuerCarTypeTableView()
        miningCarList()
    }
    
    fileprivate func miningCarList() {
//        for type in 0 ... brand.count - 1 {
//            var array = [String]()
//            for cars in carList {
//                for (key , value) in cars {
//                    if key == brand[type] {
//                        array.append(value)
//                    }
//                }
//            }
//            expandableArray.append(ExpandableNames(isExpanded: false, names: array))
//        }
        
        for type in 0 ... brand.count - 1 {
            var carNamesArray = [String]()
            var carIds = [String]()
            var brandIds = [String]()
            for car in carModelsListWithId {
                if car.brandName == brand[type] {
                    carNamesArray.append(car.carName)
                    carIds.append(car.carId)
                    brandIds.append(car.categoryId)
                }
            }
            expandableArray.append(ExpandableNames(isExpanded: false, names: carNamesArray, carNamesId: carIds, brandId: brandIds))
        }
        //print(expandableArray)
        
        
    }
    
    @objc fileprivate func dismissVC (tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.didDismissAddCarViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func configureShowChoosImage(cameraDevice : UIImagePickerController.CameraDevice){
        let showChooseImage = SelectProfilePictureVC()
        showChooseImage.cameraDevice = cameraDevice
        showChooseImage.delegate = self
        showChooseImage.modalPresentationStyle = .overCurrentContext
        showChooseImage.modalTransitionStyle = .coverVertical
        showChoosImageVC = showChooseImage
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
//        let tap = UIGestureRecognizer(target: self, action: #selector(hiddeLists))
//        view.addGestureRecognizer(tap)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    @objc func hiddeLists(_ sender: UITapGestureRecognizer? = nil) {
        //print("is clicked")
        pickerView.isHidden = true
        colorPickerView.isHidden = true
        //setView(view: myTableView, hidden: true)
        myTableView.isHidden = true
    }
    
    let titleLb : UILabel = {
        let lb = UILabel()
        lb.text = "Car Specification"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 20)
        lb.textAlignment = .center
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let licenceNumber : UILabel = {
        let lb = UILabel()
        lb.text = "Car licence number"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let licenceNumberPlate : UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "plate"))
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var leftPartTF : UITextField = {
        let tf = UITextField()
        tf.placeholder = "22"
        tf.delegate = self
        tf.backgroundColor = .white
        tf.textColor = .black
        tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        tf.textAlignment = .center
        tf.keyboardType = .numberPad
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let letterLicence : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(UIColor(named: "darkGray"), for: .normal)
        btn.setTitle("الف", for: .normal)
        btn.titleLabel?.textAlignment = .center
        btn.backgroundColor = .white
        btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: 17)
        btn.addTarget(self, action: #selector(openList(tapGestureRecognizer:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var rightPartTF : UITextField = {
        let tf = UITextField()
        tf.delegate = self
        tf.placeholder = "222"
        tf.backgroundColor = .white //UIColor(named: "gray")
        tf.textColor = .black
        tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        tf.textAlignment = .center
        tf.keyboardType = .numberPad
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    lazy var cityPartTF : UITextField = {
        let tf = UITextField()
        tf.delegate = self
        tf.placeholder = "22"
        tf.backgroundColor = .white //UIColor(named: "gray")
        tf.textColor = .black
        tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        tf.textAlignment = .center
        tf.keyboardType = .numberPad
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()

    lazy var pickerView : UIPickerView = {
        let pv = UIPickerView()
        pv.isHidden = true
        pv.backgroundColor = UIColor(named: "gray")
        pv.tintColor = UIColor(named: "blue")
        pv.delegate = self
        pv.dataSource = self
        pv.layer.masksToBounds = false
        pv.layer.shadowColor = UIColor.black.cgColor
        pv.layer.shadowOffset = CGSize(width: -1, height: 1)
        pv.layer.shadowRadius = 5
        pv.layer.shadowOpacity = 0.6
        pv.layer.shouldRasterize = true
        pv.layer.rasterizationScale = UIScreen.main.scale
        let tap = UITapGestureRecognizer(target: self, action: #selector(openList(tapGestureRecognizer:)))
        pv.addGestureRecognizer(tap)
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    @objc fileprivate func closeList(tapGestureRecognizer: UITapGestureRecognizer) {
        setView(view: pickerView, hidden: true)
    }
    
    let openListImg : UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "Triangle"))
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(openList(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func openList(tapGestureRecognizer : UITapGestureRecognizer){
        setView(view: pickerView, hidden: false)
    }
    
    let carCardFront : UILabel = {
        let lb = UILabel()
        lb.text = "Car card front page"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarCardFront : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(chooseCarCardFrontPage(tap :)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var carCardFrontImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardFrontPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backGreenPencilIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardFrontPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.tintColor = UIColor(named: "green")
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var greenPencilImageIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardFrontPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.tintColor = UIColor(named: "green")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseCarCardFrontPage(tap : UITapGestureRecognizer) {
        didChooseCarCardFrontImage = true
        if showChoosImageVC == nil {
            configureShowChoosImage(cameraDevice: UIImagePickerController.CameraDevice.rear)
            darkView.isHidden = false
            present(showChoosImageVC, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showChoosImageVC, animated: true)
        }
    }
    
    let carCardBehind : UILabel = {
        let lb = UILabel()
        lb.text = "Car card behind page"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarCardBehind : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(chooseCarCardBehindPage(tap:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var carCardBehindImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardBehindPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backGreenPencilIV2 : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardBehindPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.tintColor = UIColor(named: "green")
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var greenPencilImageIV2 : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseCarCardBehindPage(tap:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.tintColor = UIColor(named: "green")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseCarCardBehindPage(tap : UITapGestureRecognizer) {
        didChooseCarCardBehindImage = true
        if showChoosImageVC == nil {
            configureShowChoosImage(cameraDevice: UIImagePickerController.CameraDevice.rear)
            darkView.isHidden = false
            present(showChoosImageVC, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showChoosImageVC, animated: true)
        }
    }

    let carType : UILabel = {
        let lb = UILabel()
        lb.text = "Car type"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarType: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(openCarTypeList(tap:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func openCarTypeList(tap: UITapGestureRecognizer) {
        setView(view: myTableView, hidden: false)
    }
    
    let carColor : UILabel = {
        let lb = UILabel()
        lb.text = "Car color"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarColor: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(openColorList(tap:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var colorPickerView : UIPickerView = {
        let pv = UIPickerView()
        pv.isHidden = true
        pv.backgroundColor = UIColor(named: "gray")
        pv.tintColor = UIColor(named: "blue")
        pv.delegate = self
        pv.dataSource = self
        pv.layer.masksToBounds = false
        pv.layer.shadowColor = UIColor.black.cgColor
        pv.layer.shadowOffset = CGSize(width: -1, height: 1)
        pv.layer.shadowRadius = 5
        pv.layer.shadowOpacity = 0.6
        pv.layer.shouldRasterize = true
        pv.layer.rasterizationScale = UIScreen.main.scale
        let tap = UITapGestureRecognizer(target: self, action: #selector(openList(tapGestureRecognizer:)))
        pv.addGestureRecognizer(tap)
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    @objc fileprivate func openColorList(tap : UITapGestureRecognizer){
        setView(view: colorPickerView, hidden: false)
    }
    
    let carFacilities : UILabel = {
        let lb = UILabel()
        lb.text = "Car facilities"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarFacilities: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(showCarFacilities(tap:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func showCarFacilities(tap : UITapGestureRecognizer){
        darkView.isHidden = false
        if showCarFacilitiesVC == nil {
            configureCarFacilities()
            present(showCarFacilitiesVC, animated: true, completion: nil)
        } else {
            present(showCarFacilitiesVC, animated: true, completion: nil)
        }
    }
    
    fileprivate func configureCarFacilities(){
        let show = CarFacilitiesVC()
        show.delegate = self
        show.statusArray = carFacilitiesData
        show.modalPresentationStyle = .overCurrentContext
        show.modalTransitionStyle = .coverVertical
        showCarFacilitiesVC = show
    }
    
    let cancelBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Cancel", for: .normal)
        btn.backgroundColor = UIColor(named: "red")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.addTarget(self, action: #selector(dismissVC(tapGestureRecognizer:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var saveBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Save", for: .normal)
        btn.backgroundColor = UIColor(named: "blue")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.addTarget(self, action: #selector(self.saveBtnAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func saveBtnAction(){
        if checkAllFeilds() {
            dataWillSend = [    ["leftPart" : leftPartTF.text!],
                                ["letterPart" : selectedLetter],
                                ["rightPart" : rightPartTF.text!],
                                ["cityPart" : cityPartTF.text!],
                                ["frontImage" : carCardFrontImage.image!],
                                ["behindImage" : carCardBehindImage.image!],
                                ["brand" : brandPlus],
                                ["brandId" : brandID],
                                ["carId" : carID],
                                ["carType" : addCarType.titleLabel!.text!],
                                ["carColor" : addCarColor.titleLabel!.text!],
                                ["carColorId" : colorID],
                                ["carFacilities" : carFacilitiesData],
            ]
            delegate?.didDataSendBack(carData: dataWillSend)
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    fileprivate func checkAllFeilds() -> Bool{
        if leftPartTF.text!.isEmpty || leftPartTF.text?.count != 2 {
            showError(err: "Left part isn't correct!")
            return false
        } else if selectedLetter.isEmpty {
            showError(err: "letter is empty")
            return false
        } else if rightPartTF.text!.isEmpty || rightPartTF.text!.count != 3 {
            showError(err: "Right part isn't correct!")
            return false
        }  else if cityPartTF.text!.isEmpty || cityPartTF.text!.count != 2 {
            showError(err: "City part isn't correct!")
            return false
        } else if carCardFrontImage.image == nil {
            showError(err: "Car card front page is empty!")
            return false
        } else if carCardBehindImage.image == nil {
            showError(err: "Car card behind page is empty!")
            return false
        } else if addCarType.titleLabel!.text!.contains("Choose") {
            showError(err: "Car type isn't choosed")
            return false
        } else if addCarColor.titleLabel!.text!.contains("Choose"){
            showError(err: "Car color isn't choosed")
            return false
        } else if addCarFacilities.titleLabel!.text!.contains("Choose") {
            showError(err: "Car facilities isn't choosed")
            return false
        }
        // Now everything is ok
        return true
    }
    
    fileprivate func showError(err : String){
        let error = UIAlertController(title: "Error", message: err, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        present(error, animated: true, completion: nil)
    }
    
    let darkView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.3)
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate func configuerCarTypeTableView() {
        myTableView = UITableView()
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.backgroundColor = .lightGray
        myTableView.setCornerRadius(size: 24)
        myTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(myTableView)
        myTableView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        myTableView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        myTableView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        myTableView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        myTableView.isHidden = true
    }
    
    fileprivate func setupViewsLayout() {
        view.addSubview(containerView)
        containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 440).isActive = true
        
        containerView.addSubview(titleLb)
        titleLb.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8).isActive = true
        titleLb.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        titleLb.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        
        containerView.addSubview(licenceNumber)
        licenceNumber.topAnchor.constraint(equalTo: titleLb.bottomAnchor, constant: 8).isActive = true
        licenceNumber.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        
        containerView.addSubview(licenceNumberPlate)
        licenceNumberPlate.topAnchor.constraint(equalTo: licenceNumber.bottomAnchor, constant: 8).isActive = true
        licenceNumberPlate.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0).isActive = true
        licenceNumberPlate.widthAnchor.constraint(equalToConstant: 270).isActive = true
        licenceNumberPlate.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        containerView.addSubview(openListImg)
        openListImg.centerXAnchor.constraint(equalTo: licenceNumberPlate.centerXAnchor, constant: 0).isActive = true
        openListImg.bottomAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: -10).isActive = true
        openListImg.heightAnchor.constraint(equalToConstant: 15).isActive = true
        openListImg.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        containerView.addSubview(pickerView)
        pickerView.topAnchor.constraint(equalTo: openListImg.bottomAnchor, constant: 15).isActive = true
        pickerView.leadingAnchor.constraint(equalTo: openListImg.leadingAnchor , constant: -20).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: openListImg.trailingAnchor, constant: 20).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        containerView.addSubview(leftPartTF)
        leftPartTF.topAnchor.constraint(equalTo: licenceNumberPlate.topAnchor, constant: 12).isActive = true
        leftPartTF.bottomAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: -3).isActive = true
        leftPartTF.leadingAnchor.constraint(equalTo: licenceNumberPlate.leadingAnchor, constant: 40).isActive = true
        leftPartTF.trailingAnchor.constraint(equalTo: openListImg.leadingAnchor, constant: -55).isActive = true
        
        containerView.addSubview(letterLicence)
        letterLicence.topAnchor.constraint(equalTo: licenceNumberPlate.topAnchor, constant: 16).isActive = true
        letterLicence.bottomAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: -3).isActive = true
        letterLicence.leadingAnchor.constraint(equalTo: leftPartTF.trailingAnchor, constant: 5).isActive = true
        letterLicence.trailingAnchor.constraint(equalTo: openListImg.leadingAnchor, constant: -5).isActive = true
        
        containerView.addSubview(rightPartTF)
        rightPartTF.topAnchor.constraint(equalTo: licenceNumberPlate.topAnchor, constant: 12).isActive = true
        rightPartTF.bottomAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: -3).isActive = true
        rightPartTF.leadingAnchor.constraint(equalTo: openListImg.trailingAnchor, constant: 3).isActive = true
        rightPartTF.trailingAnchor.constraint(equalTo: licenceNumberPlate.trailingAnchor, constant: -73).isActive = true
        
        containerView.addSubview(cityPartTF)
        cityPartTF.topAnchor.constraint(equalTo: licenceNumberPlate.topAnchor, constant: 12).isActive = true
        cityPartTF.bottomAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: -3).isActive = true
        cityPartTF.leadingAnchor.constraint(equalTo: rightPartTF.trailingAnchor, constant: 10).isActive = true
        cityPartTF.trailingAnchor.constraint(equalTo: licenceNumberPlate.trailingAnchor, constant: -20).isActive = true
        
        containerView.addSubview(carCardFront)
        carCardFront.topAnchor.constraint(equalTo: licenceNumberPlate.bottomAnchor, constant: 24).isActive = true
        carCardFront.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        
        containerView.addSubview(addCarCardFront)
        addCarCardFront.centerYAnchor.constraint(equalTo: carCardFront.centerYAnchor).isActive = true
        addCarCardFront.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        addCarCardFront.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addCarCardFront.heightAnchor.constraint(equalToConstant: 30).isActive = true
        // car card image
        containerView.addSubview(carCardFrontImage)
        carCardFrontImage.centerYAnchor.constraint(equalTo: carCardFront.centerYAnchor).isActive = true
        carCardFrontImage.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        carCardFrontImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        carCardFrontImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // back green pencil
        containerView.addSubview(backGreenPencilIV)
        backGreenPencilIV.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        backGreenPencilIV.bottomAnchor.constraint(equalTo: carCardFrontImage.bottomAnchor).isActive = true
        backGreenPencilIV.heightAnchor.constraint(equalToConstant: 20).isActive = true
        backGreenPencilIV.widthAnchor.constraint(equalToConstant: 20).isActive = true
        // green pencil
        backGreenPencilIV.addSubview(greenPencilImageIV)
        greenPencilImageIV.topAnchor.constraint(equalTo: backGreenPencilIV.topAnchor,constant: 4).isActive = true
        greenPencilImageIV.leadingAnchor.constraint(equalTo: backGreenPencilIV.leadingAnchor, constant: 4).isActive = true
        greenPencilImageIV.trailingAnchor.constraint(equalTo: backGreenPencilIV.trailingAnchor,constant: -4).isActive = true
        greenPencilImageIV.bottomAnchor.constraint(equalTo: backGreenPencilIV.bottomAnchor, constant: -4).isActive = true
        
        containerView.addSubview(carCardBehind)
        carCardBehind.topAnchor.constraint(equalTo: carCardFront.bottomAnchor, constant: 30).isActive = true
        carCardBehind.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        
        containerView.addSubview(addCarCardBehind)
        addCarCardBehind.centerYAnchor.constraint(equalTo: carCardBehind.centerYAnchor).isActive = true
        addCarCardBehind.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        addCarCardBehind.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addCarCardBehind.heightAnchor.constraint(equalToConstant: 30).isActive = true
        // car card image
        containerView.addSubview(carCardBehindImage)
        carCardBehindImage.centerYAnchor.constraint(equalTo: carCardBehind.centerYAnchor).isActive = true
        carCardBehindImage.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        carCardBehindImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        carCardBehindImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // back green pencil
        containerView.addSubview(backGreenPencilIV2)
        backGreenPencilIV2.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        backGreenPencilIV2.bottomAnchor.constraint(equalTo: carCardBehindImage.bottomAnchor).isActive = true
        backGreenPencilIV2.heightAnchor.constraint(equalToConstant: 20).isActive = true
        backGreenPencilIV2.widthAnchor.constraint(equalToConstant: 20).isActive = true
        // green pencil
        backGreenPencilIV2.addSubview(greenPencilImageIV2)
        greenPencilImageIV2.topAnchor.constraint(equalTo: backGreenPencilIV2.topAnchor,constant: 4).isActive = true
        greenPencilImageIV2.leadingAnchor.constraint(equalTo: backGreenPencilIV2.leadingAnchor, constant: 4).isActive = true
        greenPencilImageIV2.trailingAnchor.constraint(equalTo: backGreenPencilIV2.trailingAnchor,constant: -4).isActive = true
        greenPencilImageIV2.bottomAnchor.constraint(equalTo: backGreenPencilIV2.bottomAnchor, constant: -4).isActive = true
        
        containerView.addSubview(carType)
        carType.topAnchor.constraint(equalTo: carCardBehind.bottomAnchor, constant: 30).isActive = true
        carType.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        // add car type button
        containerView.addSubview(addCarType)
        addCarType.centerYAnchor.constraint(equalTo: carType.centerYAnchor).isActive = true
        addCarType.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        addCarType.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addCarType.heightAnchor.constraint(equalToConstant: 30).isActive = true
//        // add car type picker view
//        containerView.addSubview(carTypePickerView)
//        carTypePickerView.trailingAnchor.constraint(equalTo: addCarType.leadingAnchor, constant: -10).isActive = true
//        carTypePickerView.centerYAnchor.constraint(equalTo: addCarType.centerYAnchor).isActive = true
//        carTypePickerView.widthAnchor.constraint(equalToConstant: 200).isActive = true
//        carTypePickerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        // car color label
        containerView.addSubview(carColor)
        carColor.topAnchor.constraint(equalTo: carType.bottomAnchor, constant: 30).isActive = true
        carColor.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        // add car color button
        containerView.addSubview(addCarColor)
        addCarColor.centerYAnchor.constraint(equalTo: carColor.centerYAnchor).isActive = true
        addCarColor.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        addCarColor.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addCarColor.heightAnchor.constraint(equalToConstant: 30).isActive = true
        // color list picker view
        containerView.addSubview(colorPickerView)
        colorPickerView.trailingAnchor.constraint(equalTo: addCarColor.leadingAnchor, constant: -10).isActive = true
        colorPickerView.centerYAnchor.constraint(equalTo: addCarColor.centerYAnchor).isActive = true
        colorPickerView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        colorPickerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        
        containerView.addSubview(carFacilities)
        carFacilities.topAnchor.constraint(equalTo: carColor.bottomAnchor, constant: 30).isActive = true
        carFacilities.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        
        containerView.addSubview(addCarFacilities)
        addCarFacilities.centerYAnchor.constraint(equalTo: carFacilities.centerYAnchor).isActive = true
        addCarFacilities.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24).isActive = true
        addCarFacilities.widthAnchor.constraint(equalToConstant: 70).isActive = true
        addCarFacilities.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        stack = UIStackView(arrangedSubviews: [cancelBtn, saveBtn])
        containerView.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        
        stack.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        stack.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
        // dark view
        view.addSubview(darkView)
        darkView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        darkView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        darkView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        darkView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func changeAddCarColorBackground(color : String){
        switch color {
        case "Blue" :
            addCarColor.backgroundColor = UIColor(named: "blue")
            setView(view: colorPickerView, hidden: true)
            break
        case "White" :
            addCarColor.backgroundColor = UIColor(named: "gray")
            setView(view: colorPickerView, hidden: true)
            break
        case "Black" :
            addCarColor.backgroundColor = .black
            setView(view: colorPickerView, hidden: true)
            break
        case "Red" :
            addCarColor.backgroundColor = UIColor(named: "red")
            setView(view: colorPickerView, hidden: true)
            break
        default :
            print("defualt")
        }
    }
    
}

extension AddCarVC : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == self.pickerView {
            return 1
        } else {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerView {
            return licenceData.count
        } else {
            return colorsList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pickerView {
            letterLicence.setTitle(licenceData[row], for: .normal)
            selectedLetter = licenceData[row]
            setView(view: pickerView, hidden: true)
        } else if pickerView == self.colorPickerView{
            addCarColor.setTitle(colorsList[row].colorName, for: .normal)
            colorID = colorsList[row].colorID
            changeAddCarColorBackground(color: colorsList[row].colorName)
            setView(view: colorPickerView, hidden: true)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.pickerView {
            return licenceData[row]
        } else {
            return colorsList[row].colorName
        }
    }
}

extension AddCarVC : BackFromChooseImage {
    func didBackFromChooseImage() {
        self.darkView.isHidden = true
        //showChoosImageVC.dismiss(animated: true, completion: nil)
    }
    
    func didImagePicked(pickedImg: UIImage) {
        if didChooseCarCardFrontImage {
            carCardFrontImage.isHidden = false
            addCarCardFront.isHidden = true
            backGreenPencilIV.isHidden = false
            greenPencilImageIV.isHidden = false
            carCardFrontImage.image = pickedImg
            didChooseCarCardFrontImage = false
        } else if didChooseCarCardBehindImage {
            addCarCardBehind.isHidden = true
            carCardBehindImage.isHidden = false
            backGreenPencilIV2.isHidden = false
            greenPencilImageIV2.isHidden = false
            carCardBehindImage.image = pickedImg
            didChooseCarCardBehindImage = false
        }
    }
}

extension AddCarVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.count
        if textField == self.leftPartTF {
            //print("left text field")
            if length > 2 {
                return false
            }
        } else if textField == self.rightPartTF {
            //print("right text field")
            if length > 3 {
                return false
            }
        } else if textField == self.cityPartTF {
            if length > 2 {
                return false
            }
        }
        return true
    }
}

extension AddCarVC : DismissCarFacilities {
    func didDataSelected(array: [SwitchStatusModel]) {
        //self.carFacilitiesData = [SwitchStatusModel]()
        self.carFacilitiesData = array
    }
    
    func didDismissCarFacilitiesViewController() {
        darkView.isHidden = true
        addCarFacilities.setTitle("Checked", for: .normal)
    }
}

extension AddCarVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addCarType.setTitle(expandableArray[indexPath.section].names[indexPath.row], for: .normal)
        brandPlus = brand[indexPath.section]
        brandID = expandableArray[indexPath.section].brandId[indexPath.row]
        carID = expandableArray[indexPath.section].carNamesId[indexPath.row]
        setView(view: myTableView, hidden: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return expandableArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !expandableArray[section].isExpanded {
            return 0
        }
        return expandableArray[section].names.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let name = expandableArray[indexPath.section].names[indexPath.row]
        cell.backgroundColor = UIColor(named: "gray")
        cell.textLabel?.text = name
        cell.textLabel?.font = UIFont(name: "ComicSansMS", size: 16)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.textColor = .white
        btn.setTitle(brand[section], for: .normal)
        btn.tintColor = .white
        btn.tag = section
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        btn.addTarget(self, action: #selector(openCloseList), for: .touchUpInside)
        return btn
    }
    
    @objc func openCloseList(button : UIButton) {
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in expandableArray[section].names.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        expandableArray[section].isExpanded = !expandableArray[section].isExpanded
        if expandableArray[section].isExpanded {
            myTableView.insertRows(at: indexPaths, with: .fade)
        } else {
            myTableView.deleteRows(at: indexPaths, with: .fade)
        }
        
    }
}
