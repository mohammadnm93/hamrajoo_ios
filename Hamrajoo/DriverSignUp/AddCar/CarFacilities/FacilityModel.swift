import Foundation

enum facilityOption : Int, CustomStringConvertible {
    case airConditioner
    case music
    case pet
    case bag
    case smoke
    
    var description: String {
        switch self {
        case .airConditioner:
            return "Air conditioner in trip"
        case .music:
            return "Music in trip"
        case .pet:
            return "Passengers can have pet"
        case .bag:
            return "Passengers can have bag"
        case .smoke:
            return "Passengers can smoke"
        }
    }
}

class SwitchStatusModel {
    var isSwitchOn = false
}
