import UIKit

class CarFacilitiesVC: UIViewController {
    
    var delegate : DismissCarFacilities?
    var myTableView : UITableView!
    var cellReuseIdentifier = "facilityCell"
    var statusArray = [SwitchStatusModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewsLayout()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        view.addGestureRecognizer(tap)
        configureTableView()
        fillArray()
    }
    
    fileprivate func fillArray(){
        for _ in 1 ... 5 {
            let newModel = SwitchStatusModel()
            newModel.isSwitchOn = false
            statusArray.append(newModel)
        }
    }
    
    @objc fileprivate func dismissView(){
        delegate?.didDismissCarFacilitiesViewController()
        delegate?.didDataSelected(array: statusArray)
        self.dismiss(animated: true, completion: nil)
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleLb : UILabel = {
        let lb = UILabel()
        lb.text = "Car facilities"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 22)
        lb.textAlignment = .center
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    fileprivate func setupViewsLayout(){
        // container view
        view.addSubview(containerView)
        containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -2).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        // title label
        containerView.addSubview(titleLb)
        titleLb.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 4).isActive = true
        titleLb.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        titleLb.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
    }
    
    fileprivate func configureTableView(){
        myTableView = UITableView()
        myTableView.register(FacilitiesCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        myTableView.backgroundColor = .white
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.rowHeight = 50
        myTableView.separatorStyle = .none
        myTableView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(myTableView)
        myTableView.topAnchor.constraint(equalTo: titleLb.bottomAnchor).isActive = true
        myTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        myTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        myTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

}

extension CarFacilitiesVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FacilitiesCell
        let menuOption = facilityOption(rawValue: indexPath.row)
        cell.descLb.text = menuOption?.description
        cell.delegate = self
        cell.setupModels(model: statusArray[indexPath.row])
        return cell
    }
}

extension CarFacilitiesVC : CarFacilitiesSwitchDelegate {
    func didTapedSwitch(cell: FacilitiesCell) {
        let indexPath = myTableView.indexPath(for: cell)
        statusArray[(indexPath?.row)!].isSwitchOn = cell.switchBtn.isOn
    }
}
