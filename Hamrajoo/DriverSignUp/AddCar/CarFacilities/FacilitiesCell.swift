import UIKit

class FacilitiesCell: UITableViewCell {
    
    var delegate : CarFacilitiesSwitchDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViewsLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupModels(model : SwitchStatusModel){
        switchBtn.setOn(model.isSwitchOn, animated: true)
    }
    
    let descLb : UILabel = {
        let lb = UILabel()
        lb.text = "Hello"
        lb.font = UIFont(name: "ComicSansMS", size: 18)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let switchBtn : UISwitch = {
        let sw = UISwitch()
        sw.tintColor = UIColor(named: "darkGreen")
        sw.onTintColor = UIColor(named: "lightGreen")
        sw.thumbTintColor = UIColor(named: "green")
        sw.translatesAutoresizingMaskIntoConstraints = false
        return sw
    }()
    
    @objc func didSwitched(sw : UISwitch){
        print(sw.isOn)
        delegate.didTapedSwitch(cell: self)
    }
    
    fileprivate func setupViewsLayout(){
        addSubview(descLb)
        descLb.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        descLb.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        
        addSubview(switchBtn)
        switchBtn.centerYAnchor.constraint(equalTo: descLb.centerYAnchor).isActive = true
        switchBtn.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        switchBtn.leadingAnchor.constraint(equalTo: descLb.trailingAnchor).isActive = true
        switchBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        switchBtn.addTarget(self, action: #selector(didSwitched), for: .valueChanged)
    }

}
