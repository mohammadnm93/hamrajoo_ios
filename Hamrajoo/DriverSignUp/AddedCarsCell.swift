import UIKit

class AddedCarsCell: UITableViewCell {
    
    var labelStack : UIStackView!
    var buttonsStack : UIStackView!
    var delegate : AddedCarEditDeleteDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViewsLayout()
    }
    
    func setupModel(model : CarAddedModel) {
        model.willDelete = true
        model.willEdit = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let carName : UILabel = {
        let lb = UILabel()
        lb.text = "carName"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let carColor : UILabel = {
        let lb = UILabel()
        lb.text = "carColor"
        lb.font = UIFont(name: "ComicSansMS", size: 14)
        lb.textAlignment = .center
        lb.textColor = UIColor(named: "carColor")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let carNumber : UILabel = {
        let lb = UILabel()
        lb.text = "carNumber"
        lb.font = UIFont(name: "ComicSansMS", size: 14)
        lb.textAlignment = .center
        lb.textColor = UIColor(named: "carLicence")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let deleteBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        btn.tintColor = UIColor(named: "red")
        btn.imageView?.contentMode = .scaleAspectFit
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let editBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "gEdit"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.tintColor = UIColor(named: "green")
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func deleteAction(){
        delegate.didDeleteCell(cell: self)
    }
    
    @objc fileprivate func editAct(){
        delegate.didEditCell(cell: self)
    }
    
    fileprivate func setupViewsLayout() {
        addSubview(carName)
        carName.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        carName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        
        deleteBtn.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        editBtn.addTarget(self, action: #selector(editAct), for: .touchUpInside)
        buttonsStack = UIStackView(arrangedSubviews: [deleteBtn, editBtn])
        buttonsStack.axis = .vertical
        buttonsStack.alignment = .center
        buttonsStack.distribution = .fillEqually
        buttonsStack.spacing = 8
        
        buttonsStack.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(buttonsStack)
        buttonsStack.topAnchor.constraint(equalTo: topAnchor,constant: 4).isActive = true
        buttonsStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        buttonsStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        buttonsStack.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        labelStack = UIStackView(arrangedSubviews: [carColor, carNumber])
        labelStack.axis = .vertical
        labelStack.alignment = .center
        labelStack.distribution = .fillEqually
        labelStack.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelStack)
        labelStack.topAnchor.constraint(equalTo: topAnchor).isActive = true
        labelStack.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        labelStack.trailingAnchor.constraint(equalTo: buttonsStack.leadingAnchor, constant: -4).isActive = true
        labelStack.widthAnchor.constraint(equalToConstant: 160).isActive = true
    }
}
