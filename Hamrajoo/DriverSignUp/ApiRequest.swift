//
//  RestApi.swift
//  Hamrajoo
//
//  Created by Hamrajoo on 9/3/19.
//  Copyright © 2019 Hamrajoo. All rights reserved.
//

import Foundation

enum APIError : Error {
    case responseProblem
    case decodingProblem
    case otherProblem
}

struct APIRequest {
    let resourceURL : URL
    
    init(endpoint : String) {
        let resourceString = "https://hamrajoo.net/test/v0/\(endpoint)"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
}
