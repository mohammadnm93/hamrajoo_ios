import UIKit

struct addedCarTableDataStruct {
    var carName : String
    var carColor : String
    var carNumber : String
}

struct colorsNameWithID {
    var colorName : String
    var colorID : String
}

struct carModelsWithId {
    var brandName : String
    var carName : String
    var carId : String
    var categoryId : String
}

class DriverSignUpVC: UIViewController {
    
    var showChoosImageVC : UIViewController!
    var showHowMuchYouTalk : UIViewController!
    var showAddCarDialog : UIViewController!
    var didChooseProfilePhoto = false
    var didChooseNationalCardPhoto = false
    var didChooseDriverLicencePhoto = false
    var colorList = [String]()
    var brand = [String]()
    var carModel = [[String : String]]()
    var carsTable : UITableView!
    var cellId = "addedCarCell"
    var addedCarTableData = [addedCarTableDataStruct]()
    var addedCarModel = [CarAddedModel]()
    var backCarData = [[[String : Any]]]()
    var isEditingAddedCar = false
    var myIndexPath : IndexPath!
    var colorsListWithIds = [colorsNameWithID]()
    var brandIDs = [[String : String]]()
    var carModelsListWithId = [carModelsWithId]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationView()
        view.backgroundColor = UIColor(named: "gray")
        setupViewsLayout()
        getDataFromServer()
        getColorsFromServer()
        getCarsTypeFromServer()
    }
    
    fileprivate func fillDataArray(){
        for _ in 1 ... addedCarTableData.count {
            let newModel = CarAddedModel()
            newModel.willDelete = false
            newModel.willEdit = false
            addedCarModel.append(newModel)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func configureNavigationView() {
        navigationController?.navigationBar.isTranslucent = false
        setupNavigationBarBackgroundColor()
        //right button
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "driver_reg_btn", comment: ""), style: .plain, target: self, action: #selector(SignUp_Action))
        navigationController?.navigationBar.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceLeftToRight
        navigationController?.navigationBar.tintColor = .white
        // title
        navigationItem.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "driver_reg_title", comment: "")
        // left button
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "driver_reg_cancel", comment: ""), style: .plain, target: self, action: #selector(cancelAction))
        //language and size for title and right button and left button
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "IRANSansWeb-Bold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "IRANSansWeb", size: 14.0)!], for: .normal)
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "IRANSansWeb", size: 14.0)!], for: .normal)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS-Bold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "ComicSansMS", size: 14.0)!], for: .normal)
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "ComicSansMS", size: 14.0)!], for: .normal)
        }
    }
    
    fileprivate func configureShowChoosImage(cameraDevice : UIImagePickerController.CameraDevice){
        let showChooseImage = SelectProfilePictureVC()
        showChooseImage.cameraDevice = cameraDevice
        showChooseImage.delegate = self
        showChooseImage.modalPresentationStyle = .overCurrentContext
        showChooseImage.modalTransitionStyle = .coverVertical
        showChoosImageVC = showChooseImage
    }
    
    fileprivate func configureShowHowMuchYouTalk() {
        let talkVC = HowMuchTalkVC()
        talkVC.delegate = self
        talkVC.modalPresentationStyle = .overCurrentContext
        talkVC.modalTransitionStyle = .coverVertical
        showHowMuchYouTalk = talkVC
    }
    
    func setupNavigationBarBackgroundColor(){
        if let navigationBar = self.navigationController?.navigationBar {
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [UIColor(named: "blue")?.cgColor as Any, UIColor(named: "green")?.cgColor as Any]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @objc fileprivate func SignUp_Action(){
        if checkFieldsValidity() {
            print("ok")
            sendAllDataToServer()
        } else {
            print("error")
        }
    }
    
    @objc fileprivate func cancelAction() {
        
    }
    
    let darkView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.3)
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let driverPhotoTitle : UILabel = {
        let lb = UILabel()
        lb.text = "Driver photo"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        lb.textAlignment = .left
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let topContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let askPhotoTitle : UILabel = {
        let lb = UILabel()
        lb.text = "Choose your profile photo"
        lb.font = UIFont(name: "ComicSansMS", size: 15)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    lazy var photoIV : UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "user"))
        iv.contentMode = .scaleAspectFill
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseProfilePhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backPencilIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseProfilePhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var pencilImageIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseProfilePhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseProfilePhoto(tapGestureRecognizer: UITapGestureRecognizer){
        didChooseProfilePhoto = true
        if showChoosImageVC == nil {
            configureShowChoosImage(cameraDevice: UIImagePickerController.CameraDevice.front)
            darkView.isHidden = false
            present(showChoosImageVC, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showChoosImageVC, animated: true)
        }
    }
    
    let nameLb : UILabel = {
        let lb = UILabel()
        lb.text = "Sam Moghadam"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        lb.textAlignment = .left
        lb.numberOfLines = 2
        lb.textColor = UIColor(named: "blue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let ageLb : UILabel = {
        let lb = UILabel()
        lb.text = "30 years old"
        lb.font = UIFont(name: "ComicSansMS", size: 15)
        lb.textAlignment = .left
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let phoneLb : UILabel = {
        let lb = UILabel()
        lb.text = "09122258005"
        lb.font = UIFont(name: "ComicSansMS", size: 15)
        lb.textAlignment = .left
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let driverSpecificationLb : UILabel = {
        let lb = UILabel()
        lb.text = "Driver specification"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        lb.textAlignment = .left
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let midContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let nationalCardTitleLb : UILabel = {
        let lb = UILabel()
        lb.text = "National card photo"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let getNationalCardBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(chooseNationalCarPhoto(tapGestureRecognizer:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let nationalCardImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseNationalCarPhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backGreenPencilIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseNationalCarPhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.tintColor = UIColor(named: "green")
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var greenPencilImageIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseProfilePhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.tintColor = UIColor(named: "green")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseNationalCarPhoto(tapGestureRecognizer: UITapGestureRecognizer){
        didChooseNationalCardPhoto = true
        if showChoosImageVC == nil {
            configureShowChoosImage(cameraDevice: UIImagePickerController.CameraDevice.rear)
            darkView.isHidden = false
            present(showChoosImageVC, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showChoosImageVC, animated: true)
        }
    }
    
    let driverLicenceTitleLb : UILabel = {
        let lb = UILabel()
        lb.text = "Driver licence photo"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let getDriverLicenceBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Add", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(chooseDriverLicencePhoto(tapGestureRecognizer:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let driverLicenceImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseNationalCarPhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backGreenPencil2IV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseNationalCarPhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.tintColor = UIColor(named: "green")
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var greenPencil2ImageIV : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseProfilePhoto(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.isHidden = true
        iv.tintColor = UIColor(named: "green")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseDriverLicencePhoto(tapGestureRecognizer: UITapGestureRecognizer){
        didChooseDriverLicencePhoto = true
        if showChoosImageVC == nil {
            configureShowChoosImage(cameraDevice: UIImagePickerController.CameraDevice.rear)
            darkView.isHidden = false
            present(showChoosImageVC, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showChoosImageVC, animated: true)
        }
    }
    
    let talkTitleLb : UILabel = {
        let lb = UILabel()
        lb.text = "How much you talk"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let getTalkBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 16)
        btn.setCornerRadius(size: 14)
        btn.addTarget(self, action: #selector(howMuchTalkBtnAct), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func howMuchTalkBtnAct(){
        if showHowMuchYouTalk == nil {
            darkView.isHidden = false
            configureShowHowMuchYouTalk()
            present(showHowMuchYouTalk, animated: true, completion: nil)
        } else {
            darkView.isHidden = false
            present(showHowMuchYouTalk, animated: true, completion: nil)
        }
    }
    
    let carSpecificationLb : UILabel = {
        let lb = UILabel()
        lb.text = "Car specification"
        lb.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        lb.textAlignment = .left
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let bottomContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let addCarLb : UILabel = {
        let lb = UILabel()
        lb.text = "Add car"
        lb.font = UIFont(name: "ComicSansMS", size: 16)
        lb.textAlignment = .left
        lb.textColor = UIColor(named: "darkBlue")
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let addCarBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setCornerRadius(size: 14)
        btn.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        btn.tintColor = UIColor(named: "green")
        btn.addTarget(self, action: #selector(addCarButtonAct), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func addCarButtonAct(){
        darkView.isHidden = false
        isEditingAddedCar = false
        let addCar = AddCarVC()
        addCar.delegate = self
        addCar.carList = self.carModel
        addCar.brand = self.brand
        addCar.carModelsListWithId = self.carModelsListWithId
        addCar.colorsList = self.colorsListWithIds
        
        addCar.modalPresentationStyle = .overCurrentContext
        addCar.modalTransitionStyle = .crossDissolve
        present(addCar, animated: true, completion: nil)
    }
    
    fileprivate func setupViewsLayout(){
        view.addSubview(driverPhotoTitle)
        driverPhotoTitle.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 4).isActive = true
        driverPhotoTitle.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 12).isActive = true
        driverPhotoTitle.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        // Top Container
        view.addSubview(topContainerView)
        topContainerView.topAnchor.constraint(equalTo: driverPhotoTitle.bottomAnchor, constant: 4).isActive = true
        topContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 8).isActive = true
        topContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        topContainerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        topContainerView.addSubview(askPhotoTitle)
        askPhotoTitle.topAnchor.constraint(equalTo: topContainerView.topAnchor, constant: 8).isActive = true
        askPhotoTitle.leadingAnchor.constraint(equalTo: topContainerView.leadingAnchor, constant: 16).isActive = true
        
        topContainerView.addSubview(photoIV)
        photoIV.topAnchor.constraint(equalTo: askPhotoTitle.bottomAnchor, constant: 8).isActive = true
        photoIV.leadingAnchor.constraint(equalTo: topContainerView.leadingAnchor, constant: 8).isActive = true
        photoIV.widthAnchor.constraint(equalToConstant: 100).isActive = true
        photoIV.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        topContainerView.addSubview(nameLb)
        nameLb.topAnchor.constraint(equalTo: askPhotoTitle.bottomAnchor, constant: 4).isActive = true
        nameLb.leadingAnchor.constraint(equalTo: photoIV.trailingAnchor, constant: 32).isActive = true
        nameLb.trailingAnchor.constraint(equalTo: topContainerView.trailingAnchor, constant: -8).isActive = true
        
        topContainerView.addSubview(ageLb)
        ageLb.leadingAnchor.constraint(equalTo: photoIV.trailingAnchor, constant: 32).isActive = true
        ageLb.centerYAnchor.constraint(equalTo: photoIV.centerYAnchor).isActive = true
        
        topContainerView.addSubview(phoneLb)
        phoneLb.leadingAnchor.constraint(equalTo: photoIV.trailingAnchor, constant: 32).isActive = true
        phoneLb.firstBaselineAnchor.constraint(equalTo: photoIV.lastBaselineAnchor).isActive = true
        
        topContainerView.addSubview(backPencilIV)
        backPencilIV.centerYAnchor.constraint(equalTo: photoIV.centerYAnchor, constant: 24).isActive = true
        backPencilIV.trailingAnchor.constraint(equalTo: photoIV.trailingAnchor, constant: 8).isActive = true
        backPencilIV.widthAnchor.constraint(equalToConstant: 34).isActive = true
        backPencilIV.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        backPencilIV.addSubview(pencilImageIV)
        pencilImageIV.topAnchor.constraint(equalTo: backPencilIV.topAnchor,constant: 6).isActive = true
        pencilImageIV.leadingAnchor.constraint(equalTo: backPencilIV.leadingAnchor, constant: 6).isActive = true
        pencilImageIV.trailingAnchor.constraint(equalTo: backPencilIV.trailingAnchor,constant: -6).isActive = true
        pencilImageIV.bottomAnchor.constraint(equalTo: backPencilIV.bottomAnchor, constant: -6).isActive = true
        
        view.addSubview(driverSpecificationLb)
        driverSpecificationLb.topAnchor.constraint(equalTo: topContainerView.bottomAnchor, constant: 2).isActive = true
        driverSpecificationLb.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 12).isActive = true
        driverSpecificationLb.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        
        // Mid container
        
        view.addSubview(midContainerView)
        midContainerView.topAnchor.constraint(equalTo: driverSpecificationLb.bottomAnchor, constant: 4).isActive = true
        midContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 8).isActive = true
        midContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        midContainerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        midContainerView.addSubview(nationalCardTitleLb)
        nationalCardTitleLb.topAnchor.constraint(equalTo: midContainerView.topAnchor, constant: 16).isActive = true
        nationalCardTitleLb.leadingAnchor.constraint(equalTo: midContainerView.leadingAnchor, constant: 16).isActive = true
        
        midContainerView.addSubview(getNationalCardBtn)
        getNationalCardBtn.centerYAnchor.constraint(equalTo: nationalCardTitleLb.centerYAnchor).isActive = true
        getNationalCardBtn.trailingAnchor.constraint(equalTo: midContainerView.trailingAnchor, constant: -24).isActive = true
        getNationalCardBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        getNationalCardBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        midContainerView.addSubview(nationalCardImage)
        nationalCardImage.centerYAnchor.constraint(equalTo: nationalCardTitleLb.centerYAnchor).isActive = true
        nationalCardImage.trailingAnchor.constraint(equalTo: midContainerView.trailingAnchor, constant: -24).isActive = true
        nationalCardImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        nationalCardImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        midContainerView.addSubview(backGreenPencilIV)
        backGreenPencilIV.centerYAnchor.constraint(equalTo: nationalCardTitleLb.centerYAnchor, constant: 14).isActive = true
        backGreenPencilIV.trailingAnchor.constraint(equalTo: nationalCardImage.trailingAnchor, constant: 8).isActive = true
        backGreenPencilIV.widthAnchor.constraint(equalToConstant: 20).isActive = true
        backGreenPencilIV.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        backGreenPencilIV.addSubview(greenPencilImageIV)
        greenPencilImageIV.topAnchor.constraint(equalTo: backGreenPencilIV.topAnchor,constant: 4).isActive = true
        greenPencilImageIV.leadingAnchor.constraint(equalTo: backGreenPencilIV.leadingAnchor, constant: 4).isActive = true
        greenPencilImageIV.trailingAnchor.constraint(equalTo: backGreenPencilIV.trailingAnchor,constant: -4).isActive = true
        greenPencilImageIV.bottomAnchor.constraint(equalTo: backGreenPencilIV.bottomAnchor, constant: -4).isActive = true
        
        midContainerView.addSubview(driverLicenceTitleLb)
        driverLicenceTitleLb.topAnchor.constraint(equalTo: nationalCardTitleLb.bottomAnchor, constant: 24).isActive = true
        driverLicenceTitleLb.leadingAnchor.constraint(equalTo: midContainerView.leadingAnchor, constant: 16).isActive = true
        
        midContainerView.addSubview(getDriverLicenceBtn)
        getDriverLicenceBtn.centerYAnchor.constraint(equalTo: driverLicenceTitleLb.centerYAnchor).isActive = true
        getDriverLicenceBtn.trailingAnchor.constraint(equalTo: midContainerView.trailingAnchor, constant: -24).isActive = true
        getDriverLicenceBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        getDriverLicenceBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        midContainerView.addSubview(driverLicenceImage)
        driverLicenceImage.centerYAnchor.constraint(equalTo: driverLicenceTitleLb.centerYAnchor,constant: 4).isActive = true
        driverLicenceImage.trailingAnchor.constraint(equalTo: midContainerView.trailingAnchor, constant: -24).isActive = true
        driverLicenceImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        driverLicenceImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        midContainerView.addSubview(backGreenPencil2IV)
        backGreenPencil2IV.centerYAnchor.constraint(equalTo: driverLicenceTitleLb.centerYAnchor, constant: 14).isActive = true
        backGreenPencil2IV.trailingAnchor.constraint(equalTo: driverLicenceImage.trailingAnchor, constant: 8).isActive = true
        backGreenPencil2IV.widthAnchor.constraint(equalToConstant: 20).isActive = true
        backGreenPencil2IV.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        backGreenPencil2IV.addSubview(greenPencil2ImageIV)
        greenPencil2ImageIV.topAnchor.constraint(equalTo: backGreenPencil2IV.topAnchor,constant: 4).isActive = true
        greenPencil2ImageIV.leadingAnchor.constraint(equalTo: backGreenPencil2IV.leadingAnchor, constant: 4).isActive = true
        greenPencil2ImageIV.trailingAnchor.constraint(equalTo: backGreenPencil2IV.trailingAnchor,constant: -4).isActive = true
        greenPencil2ImageIV.bottomAnchor.constraint(equalTo: backGreenPencil2IV.bottomAnchor, constant: -4).isActive = true
        
        midContainerView.addSubview(talkTitleLb)
        talkTitleLb.topAnchor.constraint(equalTo: driverLicenceTitleLb.bottomAnchor, constant: 24).isActive = true
        talkTitleLb.leadingAnchor.constraint(equalTo: midContainerView.leadingAnchor, constant: 16).isActive = true
        
        midContainerView.addSubview(getTalkBtn)
        getTalkBtn.centerYAnchor.constraint(equalTo: talkTitleLb.centerYAnchor).isActive = true
        getTalkBtn.trailingAnchor.constraint(equalTo: midContainerView.trailingAnchor, constant: -24).isActive = true
        getTalkBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        getTalkBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(carSpecificationLb)
        carSpecificationLb.topAnchor.constraint(equalTo: midContainerView.bottomAnchor, constant: 2).isActive = true
        carSpecificationLb.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 12).isActive = true
        carSpecificationLb.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        // bottomContainer
        view.addSubview(bottomContainerView)
        bottomContainerView.topAnchor.constraint(equalTo: carSpecificationLb.bottomAnchor, constant: 4).isActive = true
        bottomContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 8).isActive = true
        bottomContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        bottomContainerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        
        // Bottom Container subviews
        bottomContainerView.addSubview(addCarBtn)
        addCarBtn.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 8).isActive = true
        addCarBtn.centerXAnchor.constraint(equalTo: bottomContainerView.centerXAnchor).isActive = true
        addCarBtn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        addCarBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        bottomContainerView.addSubview(addCarLb)
        addCarLb.centerYAnchor.constraint(equalTo: addCarBtn.centerYAnchor).isActive = true
        addCarLb.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor, constant: 16).isActive = true
        
        //setup dark view layout
        view.addSubview(darkView)
        darkView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        darkView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        darkView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        darkView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    fileprivate func setPhotoCircularMask() -> UIImageView{
        let mask = UIImageView(image: UIImage(named: "mask"))
        mask.contentMode = .scaleAspectFit
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.heightAnchor.constraint(equalToConstant: 90).isActive = true
        mask.widthAnchor.constraint(equalToConstant: 90).isActive = true
        return mask
    }
    
    // get user data from server
    fileprivate func getDataFromServer(){
        self.view.isUserInteractionEnabled = false
        
        let url = URL(string: "https://hamrajoo.net/test/v0/items_drivers_profile_en.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let userid = 6
        let parameters = "user_id=\(userid)"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options:[]) as? [String : Any]
                        //print("return data : \(String(describing: json!))")
                        DispatchQueue.main.async {
                            self.view.isUserInteractionEnabled = true
                            for (key, value) in json! {
                                if (key == "Items") {
                                    if let array: [[String:Any]] = value as? [[String:Any]] {
                                        for dict in array {
                                            self.nameLb.text = (dict["first_name"] as? String)!
                                            self.nameLb.text?.append(" ")
                                            self.nameLb.text?.append((dict["last_name"] as? String)!)
                                            self.photoIV.mask = self.setPhotoCircularMask()
                                            if (dict["profile_photo"] as? String) != nil {
                                                self.photoIV.load(url: URL(string: (dict["profile_photo"] as? String)!)!)
                                            }
                                            self.phoneLb.text = dict["mobile"] as? String
                                            self.ageLb.text = String(dict["age"] as! Int)
                                            self.ageLb.text?.append(" Years old")
                                            // talk parameters remained
                                        }
                                    }
                                }
                            }
                        }
                    } catch {
                        print(error)
                    }
                    
                }
                
            }
            //print("response = \(String(describing: response))")
        }
        task.resume()
    }
    
    // get colors type from server
    fileprivate func getColorsFromServer() {
        self.view.isUserInteractionEnabled = false
        let url = URL(string: "https://hamrajoo.net/test/v0/car_color_en.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                if let data = data {
                    do {
                        let json = try! JSONSerialization.jsonObject(with: data, options:[]) as? [[String : Any]]
                        print("return data : \(String(describing: json!))")
                        DispatchQueue.main.async {
                            self.view.isUserInteractionEnabled = true
                            for colorPack in json! {
                                var colorName = ""
                                var colorId = ""
                                for (key , value) in colorPack {
                                    if key == "color_name_en" {
                                        //self.colorList.append(value as! String)
                                        //print(self.colorList)
                                        colorName = value as! String
                                    }
                                    if key == "id" {
                                        colorId = value as! String
                                    }
                                }
                                self.colorsListWithIds.append(colorsNameWithID(colorName: colorName, colorID: colorId))
                            }
                        }
                    }
                }
            }
            //print("response = \(String(describing: response))")
        }
        task.resume()
    }
    // get cars type from server
    fileprivate func getCarsTypeFromServer() {
        self.view.isUserInteractionEnabled = false
        let url = URL(string: "https://hamrajoo.net/test/v0/car_model_en.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                if let data = data {
                    do {
                        DispatchQueue.main.async {
                            let json = try! JSONSerialization.jsonObject(with: data, options:[]) as? [[String : Any]]
                            //print("return data : \(String(describing: json!))")
                            self.view.isUserInteractionEnabled = true
                            var brandCount = -1
                            for carTypePack in json! {
                                for (key , value) in carTypePack {
                                    if key == "category" {
                                        self.brand.append(value as! String)
                                        brandCount += 1
                                    }
                                }
                                
                                for (key , value) in carTypePack {
                                    if key == "data" {
                                        for car in value as! [[String : Any]] {
//                                            self.carModel.append([self.brand[brandCount] : car["name_en"] as! String])
                                            
                                            self.carModelsListWithId.append(carModelsWithId(
                                                brandName: self.brand[brandCount],
                                                carName: car["name_en"] as! String,
                                                carId: car["id"] as! String,
                                                categoryId: car["car_model_id"] as! String))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //print("response = \(String(describing: response))")
        }
        task.resume()
    }
    // send all data to server
    fileprivate func sendAllDataToServer() {
        self.view.isUserInteractionEnabled = false
        let url = URL(string: "https://hamrajoo.net/test/v0/insert_drivers_info.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters = prepareDataToSend()
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        print(parameters.data(using: String.Encoding.utf8)!)
        
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: []) //JSONSerialization(withJSONObject: parameters, options: .prettyPrinted)
//            print(String(bytes: request.httpBody!, encoding: .utf8)!)
//        } catch let error {
//            print(error.localizedDescription)
//        }
        
      request.timeoutInterval = 10
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                let returnData = String(data: data!, encoding: .utf8)
                print("return data : \(String(describing: returnData))")
                //self.view.isUserInteractionEnabled = true
                //self.whichVcShouldShow(result: returnData!)
            }
            print("response = \(String(describing: response))")
            
        }
        task.resume()
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    fileprivate func prepareDataToSend() -> String {
        let userId = "6"
        let talkId = "2"
        let profileImageNameEmpty = "0"
        let profileImageName = generateImageName()
        let nationalCardImageName = generateImageName()
        let driverLicenceImageName = generateImageName()
        let profileImageEmpty = "0"
        let nationalCodeImage = "11111"//changeImageToBase64(image: nationalCardImage.image!)
        let driverLicencePhoto = "22222"//changeImageToBase64(image: driverLicenceImage.image!)
        let carCardFrontName = generateImageName()
        let carCardBehindName = generateImageName()
        
        var leftPart = ""
        var letterPart = ""
        var rightPart = ""
        var cityPart = ""
        var frontImage = UIImage()
        var behindImage = UIImage()
        //var brand = ""
        var brandID = ""
        var carID = ""
        var carType = ""
        var carColor = ""
        var carColorID = ""
        var carFacilities = [SwitchStatusModel]()
        
        var cooler = ""
        var music  = ""
        var pet    = ""
        var truck  = ""
        var smoke  = ""
        
        var carDataToSend = [[String : Any]]()
        var allCarsData = [carDataToSend]
        
        for cars in backCarData {
            for car in cars {
                for (key, value) in car {
                    if key == "leftPart" {
                        leftPart = value as! String
                        carDataToSend.append(["txt1" : leftPart])
                    }
                    if key == "letterPart" {
                        letterPart = value as! String
                        carDataToSend.append(["txt2" : letterPart])
                    }
                    if key == "rightPart" {
                        rightPart = value as! String
                        carDataToSend.append(["txt3" : rightPart])
                    }
                    if key == "cityPart" {
                        cityPart = value as! String
                        carDataToSend.append(["txt4" : cityPart])
                    }
                    if key == "frontImage" {
                        frontImage = value as! UIImage
                        carDataToSend.append(["car_image_1" : "33333"])//changeImageToBase64(image: frontImage)])
                    }
                    if key == "behindImage" {
                        behindImage = value as! UIImage
                        carDataToSend.append(["car_image_2" : "44444"])//changeImageToBase64(image: behindImage)])
                    }
                    if key == "brandId" {
                        brandID = value as! String
                        carDataToSend.append(["car_model_id" : brandID])
                    }
                    if key == "carId" {
                        carID = value as! String
                        carDataToSend.append(["sub_car_model_id" : carID])
                    }
                    if key == "carType" {
                        carType = value as! String
                        carDataToSend.append(["car_name" : carType])
                    }
                    if key == "carColor" {
                        carColor = value as! String
                        carDataToSend.append(["color_name" : carColor])
                    }
                    if key == "carColorId" {
                        carColorID = value as! String
                        carDataToSend.append(["color_id" : carColorID])
                    }
                    if key == "carFacilities" {
                        carFacilities = value as! [SwitchStatusModel]
                        cooler = carFacilities[0].isSwitchOn ? "1" : "0"
                        music  = carFacilities[1].isSwitchOn ? "1" : "0"
                        pet    = carFacilities[2].isSwitchOn ? "1" : "0"
                        truck  = carFacilities[3].isSwitchOn ? "1" : "0"
                        smoke  = carFacilities[4].isSwitchOn ? "1" : "0"
                        carDataToSend.append(["cooler" : cooler])
                        carDataToSend.append(["music" : music])
                        carDataToSend.append(["pet" : pet])
                        carDataToSend.append(["trunk" : truck])
                        carDataToSend.append(["cigarette" : smoke])
                        carDataToSend.append(["car_image_1_name" : carCardFrontName])
                        carDataToSend.append(["car_image_1_name" : carCardBehindName])
                    }
                }
            }
            allCarsData.append(carDataToSend)
        }
        
        let sendDataArray = [[
            "user_id" : userId,
            "talk_id" : talkId,
            "profile_image_name" : profileImageNameEmpty,
            "national_code_image_name" : nationalCardImageName,
            "car_licence_image_name" : driverLicenceImageName,
            "profile_image" : profileImageEmpty,
            "national_code_image" : nationalCodeImage,
            "car_licence_image" : driverLicencePhoto,
            "driver_info_cars" : ["cooler" : cooler, "music" : music ,"pet" : pet,"trunk" : truck , "cigarette" : smoke, "car_image_1_name" : carCardFrontName, "car_image_2_name" : carCardBehindName, "color_id" : carColorID, "color_name" : carColor, "car_name" : carType, "sub_car_model_id" : carID, "car_model_id" : brandID, "car_image_2" : "44434343", "car_image_1" : "344444444" , "txt4" : cityPart, "txt3" : rightPart, "txt2" : letterPart, "txt1" : leftPart]
            ]]
        let mycar = [["cooler" : cooler, "music" : music ,"pet" : pet,"trunk" : truck , "cigarette" : smoke, "car_image_1_name" : carCardFrontName, "car_image_2_name" : carCardBehindName, "color_id" : carColorID, "color_name" : carColor, "car_name" : carType, "sub_car_model_id" : carID, "car_model_id" : brandID, "car_image_2" : "44434343", "car_image_1" : "344444444" , "txt4" : cityPart, "txt3" : rightPart, "txt2" : letterPart, "txt1" : leftPart]]
        
        var myCarData : Data
        var allData : String = ""
        do {
            myCarData = try JSONSerialization.data(withJSONObject: mycar, options: .prettyPrinted)
            print(String(data: myCarData, encoding: .utf8)!)
            allData = "user_id=\(userId)&talk_id=\(talkId)&profile_image_name=\(profileImageNameEmpty)&national_code_image_name=\(nationalCardImageName)&car_licence_image_name=\(driverLicenceImageName)&profile_image=\(profileImageEmpty)&national_code_image=\(nationalCodeImage)&car_licence_image=\(driverLicencePhoto)&driver_info_cars=\(String(describing: String(data: myCarData, encoding: .utf8)))"
            return allData
        } catch let error {
            print(error.localizedDescription)
        }
        
        
        
        //print(sendDataArray)
        
        return allData
    }
    
    func changeImageToBase64(image : UIImage) -> String {
        let imageData = image.jpegData(compressionQuality: 0.5)?.base64EncodedString()
        return imageData!
    }
    
    func checkFieldsValidity() -> Bool{
        if nationalCardImage.image == nil {
            showError(msg: "Missing national card image!")
            return false
        } else if driverLicenceImage.image == nil {
            showError(msg: "Missing driver licence photo!")
            return false
        } else if (getTalkBtn.titleLabel?.text?.contains("Choose"))!{
            showError(msg: "Missing how much you talk!")
            return false
        } else if addedCarTableData.isEmpty {
            showError(msg: "Missing car added!")
            return false
        }
        return true
    }
    
    func generateImageName() -> String{
        var imageName = "photo_"
        imageName.append(randomString(length: 20))
        imageName.append("_")
        imageName.append(randomString(length: 20))
        imageName.append(".jpg")
        return imageName
    }
    
    func showError(msg : String) {
        let error = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        present(error, animated: true, completion: nil)
    }
    
    fileprivate func configureCarsTableView(){
        carsTable = UITableView()
        carsTable.register(AddedCarsCell.self, forCellReuseIdentifier: cellId)
        carsTable.dataSource = self
        carsTable.delegate = self
        carsTable.backgroundColor = .white
        carsTable.separatorStyle = .none
        carsTable.isHidden = true
        carsTable.rowHeight = 60
        carsTable.allowsSelection = false
        
        fillDataArray()
        
        carsTable.translatesAutoresizingMaskIntoConstraints = false
        bottomContainerView.addSubview(carsTable)
        carsTable.leadingAnchor.constraint(equalTo: bottomContainerView.leadingAnchor).isActive = true
        carsTable.trailingAnchor.constraint(equalTo: bottomContainerView.trailingAnchor).isActive = true
        carsTable.topAnchor.constraint(equalTo: addCarBtn.bottomAnchor).isActive = true
        carsTable.bottomAnchor.constraint(equalTo: bottomContainerView.bottomAnchor).isActive = true
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }

}

extension DriverSignUpVC : BackFromChooseImage {
    func didBackFromChooseImage() {
        self.darkView.isHidden = true
        //showChoosImageVC.dismiss(animated: true, completion: nil)
    }
    
    func didImagePicked(pickedImg: UIImage) {
        if didChooseProfilePhoto {
            photoIV.mask = self.setPhotoCircularMask()
            photoIV.image = pickedImg
            didChooseProfilePhoto = false
        } else if didChooseNationalCardPhoto {
            nationalCardImage.isHidden = false
            getNationalCardBtn.isHidden = true
            backGreenPencilIV.isHidden = false
            greenPencilImageIV.isHidden = false
            nationalCardImage.image = pickedImg
            didChooseNationalCardPhoto = false
        } else if didChooseDriverLicencePhoto {
            driverLicenceImage.isHidden = false
            backGreenPencil2IV.isHidden = false
            greenPencil2ImageIV.isHidden = false
            getDriverLicenceBtn.isHidden = true
            didChooseDriverLicencePhoto = false
            driverLicenceImage.image = pickedImg
        }
        
    }
}

extension DriverSignUpVC : DismissHowMuchYouTalk {
    func didRadioGroupSelected(index: Int) {
        switch index {
        case 0 :
            self.getTalkBtn.setTitle("Little", for: .normal)
        case 1 :
            self.getTalkBtn.setTitle("Normal", for: .normal)
        case 2 :
            self.getTalkBtn.setTitle("Much", for: .normal)
        default:
            print("default")
        }
    }
    
    func didDismissHowMuchYouTalkVC() {
        darkView.isHidden = true
    }
}

extension DriverSignUpVC : DismissAddCarVC {
    func didDataSendBack(carData: [[String : Any]]) {
        if !isEditingAddedCar {
            backCarData.append(carData)
            //print(carData)
            darkView.isHidden = true
            var carName = ""
            var carColor = ""
            var carNumber = ""
            var brand = ""
            
            for car in carData {
                for (key, value) in car {
                    if key == "carType" {
                        carName = value as! String
                        //print(carName)
                    }
                    if key == "carColor" {
                        carColor = value as! String
                        //print(carColor)
                    }
                    if key == "brand" {
                        brand = value as! String
                    }
                    if key == "leftPart" {
                        carNumber = value as! String
                    }
                    if key == "letterPart" {
                        carNumber.append(" \(value as! String)")
                    }
                    if key == "rightPart" {
                        carNumber.append(" \(value as! String)")
                    }
                    if key == "cityPart" {
                        carNumber.append(" : \(value as! String)")
                        //print(carNumber)
                    }
                }
            }
            addedCarTableData.append(addedCarTableDataStruct(carName: "\(brand) \(carName)", carColor: carColor, carNumber: carNumber))
            if carsTable == nil {
                configureCarsTableView()
                self.setView(view: carsTable, hidden: false)
            } else {
                fillDataArray()
                carsTable.reloadData()
            }
        } else {
            darkView.isHidden = true
            var carName = ""
            var carNumber = ""
            var brand = ""
            backCarData[myIndexPath.row] = carData
            for car in carData {
                for (key, value) in car {
                    if key == "carType" {
                        carName = value as! String                        
                    }
                    if key == "carColor" {
                        addedCarTableData[myIndexPath.row].carColor = value as! String
                    }
                    if key == "brand" {
                        brand = value as! String
                    }
                    if key == "leftPart" {
                        carNumber = value as! String
                    }
                    if key == "letterPart" {
                        carNumber.append(" \(value as! String)")
                    }
                    if key == "rightPart" {
                        carNumber.append(" \(value as! String)")
                    }
                    if key == "cityPart" {
                        carNumber.append(" : \(value as! String)")
                        //print(carNumber)
                        addedCarTableData[myIndexPath.row].carNumber = carNumber
                    }
                }
            }
            addedCarTableData[myIndexPath.row].carName = "\(brand) \(carName)"
            carsTable.reloadData()
        }
        
    }
    
    func didDismissAddCarViewController() {
        darkView.isHidden = true
    }
}

extension DriverSignUpVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addedCarTableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! AddedCarsCell
        cell.setupModel(model: addedCarModel[indexPath.row])
        cell.delegate = self
        cell.carName.text = "\(indexPath.row + 1) - \(addedCarTableData[indexPath.row].carName)"
        cell.carColor.text = addedCarTableData[indexPath.row].carColor
        cell.carNumber.text = addedCarTableData[indexPath.row].carNumber
        cell.backgroundColor = .white
        return cell
    }
}

extension DriverSignUpVC : AddedCarEditDeleteDelegate {
    func didEditCell(cell: AddedCarsCell) {
        let indexPath = carsTable.indexPath(for: cell)
        myIndexPath = indexPath
        //print("edit - \(indexPath!.row)")
        isEditingAddedCar = true
        let addCar = AddCarVC()
        addCar.delegate = self
        addCar.colorsList = self.colorsListWithIds
        addCar.carList = self.carModel
        addCar.brand = self.brand
        addCar.modalPresentationStyle = .overCurrentContext
        addCar.modalTransitionStyle = .crossDissolve
        
        for car in backCarData[(indexPath?.row)!] {
            for (key, value) in car {
                if key == "carType" {
                    addCar.addCarType.setTitle(value as? String, for: .normal)
                }
                if key == "carColor" {
                    addCar.addCarColor.setTitle(value as? String, for: .normal)
                    addCar.changeAddCarColorBackground(color: (value as? String)!)
                }
                if key == "leftPart" {
                    addCar.leftPartTF.text = value as? String
                }
                if key == "letterPart" {
                    addCar.letterLicence.setTitle(value as? String, for: .normal)
                    addCar.selectedLetter = value as! String
                }
                if key == "rightPart" {
                    addCar.rightPartTF.text = value as? String
                }
                if key == "cityPart" {
                    addCar.cityPartTF.text = value as? String
                }
                if key == "brand" {
                    addCar.brandPlus = (value as? String)!
                }
                if key == "frontImage" {
                    addCar.carCardFrontImage.image = (value as! UIImage)
                    addCar.addCarCardFront.isHidden = true
                    addCar.carCardFrontImage.isHidden = false
                    addCar.backGreenPencilIV.isHidden = false
                    addCar.greenPencilImageIV.isHidden = false
                }
                if key == "behindImage" {
                    addCar.carCardBehindImage.image = (value as! UIImage)
                    addCar.addCarCardBehind.isHidden = true
                    addCar.carCardBehindImage.isHidden = false
                    addCar.backGreenPencilIV2.isHidden = false
                    addCar.greenPencilImageIV2.isHidden = false
                }
                if key == "carFacilities" {
                    addCar.addCarFacilities.setTitle("Checked", for: .normal)
                    addCar.carFacilitiesData = value as! [SwitchStatusModel]
                }
            }
        }
        darkView.isHidden = false
        present(addCar, animated: true, completion: nil)
    }
    
    func didDeleteCell(cell: AddedCarsCell) {
        let indexPath = carsTable.indexPath(for: cell)
        //print("delete - \(indexPath!.row)")
        showDialog(indexPath: indexPath!)
    }
    
    func showDialog(indexPath : IndexPath) {
        let deleteDialog = UIAlertController(title: "Delete", message: "Are you sure", preferredStyle: .alert)
        let yesBtn = UIAlertAction(title: "yes", style: .default) { (_) in
            self.addedCarTableData.remove(at: indexPath.row)
            self.carsTable.deleteRows(at: [indexPath], with: .left)
            self.carsTable.reloadData()
            deleteDialog.dismiss(animated: true, completion: nil)
        }
        let noBtn = UIAlertAction(title: "No", style: .cancel) { (_) in
            deleteDialog.dismiss(animated: true, completion: nil)
        }
        deleteDialog.addAction(yesBtn)
        deleteDialog.addAction(noBtn)
        present(deleteDialog, animated: true, completion: nil)
    }
}
