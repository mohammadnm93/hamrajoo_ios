import UIKit

class ContainerController: UIViewController {
    
    var menuController : UIViewController!
    var centerController : UIViewController!
    var isExpanded : Bool = false
    var indicator: UIActivityIndicatorView!
    
    var firstName : String!
    var totoalWallet : String!
    var imageViewURL : URL!
    var status : Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeController()
        configureLoadView()
        getDataFromServer()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureHomeController(){
        let mainPage = MainPageVC()
        mainPage.delegate = self
        centerController = UINavigationController(rootViewController: mainPage)
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func configureMenuController(){
        if menuController == nil {
            let menu = MenuController()
            menu.nameLabel.text = firstName
            if imageViewURL != nil {
                menu.profileIV.load(url: imageViewURL)
            } else {
                menu.profileIV.image = UIImage(named: "user")
            }
            menu.typeLabel.text = status == 1 ? LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_passenger", comment: "") : LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_driver", comment: "")
            menu.totalWallet = self.totoalWallet

            menuController = menu
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    func showMenuController(shouldExpand : Bool) {
        if shouldExpand {
            //show menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
                    self.centerController.view.frame.origin.x = -(self.centerController.view.frame.width - 80)
                } else {
                    LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                    self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
                }
            }, completion: nil)
        } else {
            //hide menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }, completion: nil)
        }
    }
    
    fileprivate func configureLoadView(){
        indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        indicator.frame = CGRect(x: 0, y: 0, width: 500, height: 500)
        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    fileprivate func getDataFromServer(){
        
        indicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        let url = URL(string: "https://hamrajoo.net/test/v0/menu_user_info.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let userid = 6
        let parameters = "user_id=\(userid)"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
                        print("return data : \(String(describing: json!))")
                        DispatchQueue.main.async {
                            self.indicator.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        }
                        self.firstName = json!["first_name"] as? String
                        self.firstName.append(" ")
                        self.firstName.append((json!["last_name"] as? String)!)
                        self.totoalWallet = json!["total_wallet"] as? String
                        let imageurl = json!["profile_photo"] as? String
                        self.imageViewURL = URL(string: imageurl!)
                        let statusStr = json!["status"] as? String
                        self.status = Int(statusStr!)
                    
                    } catch {
                        print(error)
                    }
                    
                }
                
            }
            print("response = \(String(describing: response))")
            
        }
        task.resume()
    }

}

extension ContainerController : HomeControllerDelegate {
    func didHandleMenuToggle() {
        if !isExpanded {
            configureMenuController()
        }
        isExpanded = !isExpanded
        showMenuController(shouldExpand: isExpanded)
    }
}

extension UIImageView {
    func load(url : URL){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
