import Foundation
import UIKit

enum MenuOption : Int, CustomStringConvertible {
    case wallet
    case transactions
    case myRides
    case announcement
    case aboutUs
    case tellUs
    case settings
    case messages
    
    var description: String {
        switch self {
        case .wallet:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_wallet", comment: "")
        case .transactions:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_transactions", comment: "")
        case .myRides:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_myRides", comment: "")
        case .announcement:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_announcement", comment: "")
        case .aboutUs:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_aboutUs", comment: "")
        case .tellUs:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_tellUs", comment: "")
        case .settings:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_settinngs", comment: "")
        case .messages:
            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_messages", comment: "")
        }
    }
    
    var image: UIImage {
        switch self {
        case .wallet:
            return UIImage(named: "wallet") ?? UIImage()
        case .transactions:
            return UIImage(named: "transaction") ?? UIImage()
        case .myRides:
            return UIImage(named: "myRides") ?? UIImage()
        case .announcement:
            return UIImage(named: "announcement") ?? UIImage()
        case .aboutUs:
            return UIImage(named: "aboutUs") ?? UIImage()
        case .tellUs:
            return UIImage(named: "tellUs") ?? UIImage()
        case .settings:
            return UIImage(named: "settings") ?? UIImage()
        case .messages:
            return UIImage(named: "messages") ?? UIImage()
        }
    }
}
