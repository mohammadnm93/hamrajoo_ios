import UIKit

class MainPageVC: UIViewController {
    
    var delegate : HomeControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupNavigationBarBackgroundColor()
        setupHamrajooLogo()
        setupNotificationButton()
        setupMenuIcon()
        topContainerLayout()
        setupButtons()
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func setupNavigationBarBackgroundColor(){
        if let navigationBar = self.navigationController?.navigationBar {
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [UIColor(named: "blue")?.cgColor as Any, UIColor(named: "green")?.cgColor as Any]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    func setupHamrajooLogo(){
        let imageView =  UIImageView(frame: CGRect(x: 0, y: 0 , width: 45, height: 40))
        imageView.image = UIImage(named: "logo")
        navigationItem.titleView = imageView
    }
    
    func setupMenuIcon(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-"), style: .plain, target: self, action: #selector(menuAct))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    func setupNotificationButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(notificationAct))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
    }
    
    @objc func notificationAct() {
        //delegate?.didHandleMenuToggle()
    }
    
    @objc func menuAct(){
        delegate?.didHandleMenuToggle()
    }
    
    let titleTV : UITextView = {
        let tv = UITextView()
        tv.backgroundColor = .white
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "mainPage_title", comment: "")
        tv.textColor = UIColor(named: "blue")
        tv.textAlignment = .center
        tv.isSelectable = false
        tv.isScrollEnabled = false
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: 35)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: 35)
        }
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let descTV : UITextView = {
        let tv = UITextView()
        tv.backgroundColor = .white
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "mainPage_desc", comment: "")
        tv.textColor = UIColor(named: "blue")
        tv.textAlignment = .center
        tv.isSelectable = false
        tv.isScrollEnabled = false
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: 15)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: 15)
        }
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let driverBtn : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor(named: "blue")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "mainPage_driver", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: 22)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 22)
        }
        btn.titleLabel?.textColor = .white
        btn.titleLabel?.textAlignment = .center
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(driverBtnClicked), for: .touchUpInside)
        return btn
    }()
    
    let passengerBtn : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "mainPage_passenger", comment: ""), for: .normal)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: 26)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 26)
        }
        btn.titleLabel?.textColor = .white
        btn.titleLabel?.textAlignment = .center
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(passengerBtnClicked), for: .touchUpInside)
        return btn
    }()
    
    @objc func driverBtnClicked(){
        
    }
    
    @objc func passengerBtnClicked(){
        let passengerVC = MapTestVC()
        present(passengerVC,animated: true)
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate func topContainerLayout(){
        view.addSubview(containerView)
        let stack = UIStackView(arrangedSubviews: [titleTV,descTV])
        containerView.addSubview(stack)
        stack.backgroundColor = .red
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.setCustomSpacing(8, after: titleTV)
        
        containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.4).isActive = true
        stack.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        stack.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
    
    fileprivate func setupButtons(){
        let stack = UIStackView(arrangedSubviews: [driverBtn,passengerBtn])
        stack.backgroundColor = .orange
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.setCustomSpacing(16, after: driverBtn)
        view.addSubview(stack)
        
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 32).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -32).isActive = true
        stack.topAnchor.constraint(equalTo: containerView.bottomAnchor,constant: 16).isActive = true
        stack.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.2).isActive = true
        
        DispatchQueue.main.async {
            self.driverBtn.setCornerRadius(size: self.driverBtn.frame.height/2)
            self.passengerBtn.setCornerRadius(size: self.passengerBtn.frame.height/2)
        }
        
    }
}


