import UIKit

class MenuCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        setupViewsLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let iconIV : UIImageView = {
        let iv = UIImageView(image: UIImage(named: "wallet"))
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLb : UILabel = {
        let lb = UILabel()
        lb.text = "Wallet"
        lb.textColor = UIColor(named: "menu")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            lb.font = UIFont(name: "IRANSansWeb", size: 17)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            lb.font = UIFont(name: "ComicSansMS", size: 18)
        }
        lb.textAlignment = .left
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    fileprivate func setupViewsLayout() {
        addSubview(iconIV)
        iconIV.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconIV.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        iconIV.widthAnchor.constraint(equalToConstant: 34).isActive = true
        iconIV.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        addSubview(titleLb)
        titleLb.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleLb.leadingAnchor.constraint(equalTo: iconIV.trailingAnchor, constant: 16).isActive = true
    }
    
}
