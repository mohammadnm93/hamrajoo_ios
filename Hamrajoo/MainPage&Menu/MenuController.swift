import UIKit

class MenuController: UIViewController {
    
    var myTableView : UITableView!
    let cellReuseIdentifier = "MenuCell"
    
    var totalWallet : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupTopContainerViewLayout()
        configureTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        topCV.setGradientBackground()
    }
    
    let topCV : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var profileIV : UIImageView = {
        let iv = UIImageView()
        let mask = UIImageView(image: UIImage(named: "mask"))
        mask.contentMode = .scaleAspectFit
        iv.mask = mask
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var nameLabel : UILabel = {
        let lb = UILabel()
        lb.textColor = .white
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            lb.font = UIFont(name: "IRANSansWeb-Bold", size: 19)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            lb.font = UIFont(name: "ComicSansMS-Bold", size: 20)
        }
        lb.textAlignment = .center
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    lazy var typeLabel : UILabel = {
        let lb = UILabel()
        lb.textColor = .white
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
           lb.font = UIFont(name: "IRANSansWeb", size: 16)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            lb.font = UIFont(name: "ComicSansMS", size: 17)
        }
        lb.textAlignment = .left
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let infoLabel : UILabel = {
        let lb = UILabel()
        lb.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_moreInfo", comment: "")
        lb.textColor = .gray
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            lb.font = UIFont(name: "IRANSansWeb", size: 11)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            lb.font = UIFont(name: "ComicSansMS", size: 12)
        }
        lb.textAlignment = .left
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    fileprivate func setupTopContainerViewLayout(){
        view.addSubview(topCV)
        topCV.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topCV.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topCV.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        topCV.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        topCV.addSubview(profileIV)
        profileIV.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            profileIV.centerXAnchor.constraint(equalTo: topCV.centerXAnchor, constant: +40).isActive = true
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            profileIV.centerXAnchor.constraint(equalTo: topCV.centerXAnchor, constant: -40).isActive = true
        }
        profileIV.widthAnchor.constraint(equalToConstant: 100).isActive = true
        profileIV.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        topCV.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: profileIV.bottomAnchor, constant: 8).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: profileIV.centerXAnchor).isActive = true
        
        topCV.addSubview(typeLabel)
        typeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 12).isActive = true
        typeLabel.leadingAnchor.constraint(equalTo: topCV.leadingAnchor, constant: 8).isActive = true
        
        topCV.addSubview(infoLabel)
        infoLabel.topAnchor.constraint(equalTo: typeLabel.bottomAnchor, constant: 8).isActive = true
        infoLabel.leadingAnchor.constraint(equalTo: topCV.leadingAnchor, constant: 8).isActive = true
    }
    
    fileprivate func configureTableView() {
        myTableView = UITableView()
        myTableView.register(MenuCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.backgroundColor = .white
        myTableView.rowHeight = 60
        myTableView.separatorStyle = .none
        myTableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(myTableView)
        myTableView.topAnchor.constraint(equalTo: topCV.bottomAnchor).isActive = true
        myTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        myTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        myTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
}

extension MenuController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MenuCell
        let menuOption = MenuOption(rawValue: indexPath.row)
        cell.iconIV.image = menuOption?.image
        cell.titleLb.text = menuOption?.description
        if indexPath.row == 0 {
            cell.titleLb.text?.append("    ")
            cell.titleLb.text?.append(totalWallet)
            if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
                cell.titleLb.text?.append(" تومان")
            } else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                cell.titleLb.text?.append(" Toman")
            }
        }
        return cell
    }
}
