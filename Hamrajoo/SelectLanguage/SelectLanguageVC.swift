import UIKit

class SelectLanguageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.setGradientBackground()
        setupBottomStackViewLayout()
    }
    
    let englishButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("English", for: .normal)
        btn.setTitleColor(UIColor(red: 7/255, green: 55/255, blue: 94/255, alpha: 1), for: .normal)
        btn.backgroundColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleEnglish), for: .touchUpInside)
        btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 40)
        return btn
    }()
    
    @objc private func handleEnglish(){
        LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let swipingVC = SwipingController(collectionViewLayout: layout)
        swipingVC.modalTransitionStyle = .flipHorizontal
        present(swipingVC, animated: true)
    }
    
    let persianButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("فارسی", for: .normal)
        btn.setTitleColor(UIColor(named: "green"), for: .normal)
        btn.backgroundColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: 40)
        btn.addTarget(self, action: #selector(handlePersian), for: .touchUpInside)
        return btn
    }()
    
    @objc private func handlePersian(){
        LocalizationSystem.sharedInstance.setLanguage(languageCode: "fa")
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        print(LocalizationSystem.sharedInstance.getLanguage())

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let swipingVC = SwipingController(collectionViewLayout: layout)
        swipingVC.modalTransitionStyle = .flipHorizontal
        
        present(swipingVC, animated: true)
    }
    
    fileprivate func setupBottomStackViewLayout(){
        let stack = UIStackView(arrangedSubviews: [englishButton,persianButton])
        stack.backgroundColor = .gray
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.setCustomSpacing(20, after: englishButton)
        view.addSubview(stack)
        
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 64).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -64).isActive = true
        stack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        stack.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        
        englishButton.setCornerRadius(size: 40)
        persianButton.setCornerRadius(size: 40)
    }

}
