import UIKit

class SignUpVC: UIViewController, URLSessionDelegate {
    
    var phoneNumber : String!
    var firstNameText : String!
    var lastNameText : String!
    var internationalCode : String!
    var emailAddress : String!
    var birthDate : String!
    var userID : String = ""
    var dateText : String?
    var imageName : String?
    var imageData : String!
    
    var stack : UIStackView!
    
    let userProfileTextView : UITextView = {
        let tv = UITextView()
        tv.isEditable = false
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.textColor = .black
        tv.backgroundColor = .clear
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "userProfileTv", comment: "")
        tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize + 6)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize + 6)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize + 6)
            tv.textAlignment = .left
        }
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var backProfileImage : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "backProfileImage")
        img.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseImage(tapGestureRecognizer:)))
        img.addGestureRecognizer(tap)
        img.isUserInteractionEnabled = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    lazy var profileImage : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "user")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseImage(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var backPencil : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "backProfileImage")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseImage(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var pencilImage : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pencil")
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(chooseImage(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    @objc fileprivate func chooseImage(tapGestureRecognizer: UITapGestureRecognizer){
        darkView.isHidden = false
        datePickerContainer.isHidden = true
        datePicker.isHidden = true
        confirmButton.isHidden = true
        cancelButton.isHidden = true
        let showChooseImage = SelectProfilePictureVC()
        showChooseImage.modalPresentationStyle = .overCurrentContext
        showChooseImage.modalTransitionStyle = .coverVertical
        showChooseImage.delegate = self
        present(showChooseImage, animated: true)
    }
    
    let descriptionTV : UITextView = {
        let tv = UITextView()
        tv.isScrollEnabled = false
        tv.isSelectable = false
        tv.isEditable = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "profilePhotoDesc", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize + 2)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize + 2)
        }
        tv.textColor = UIColor(named: "darkBlue")
        tv.backgroundColor = .clear
        tv.sizeToFit()
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()

    let in_firstName: LeftPaddedTextField = {
        let tf = LeftPaddedTextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_firstName_ph", comment: "")
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
            tf.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
            tf.textAlignment = .left
        }
        tf.sizeToFit()
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let in_lastName : LeftPaddedTextField = {
        let tf = LeftPaddedTextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_lastName_ph", comment: "")
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
            tf.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
            tf.textAlignment = .left
        }
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let in_internationalCode: LeftPaddedTextField = {
        let tf = LeftPaddedTextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_nationalCode", comment: "")
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
            tf.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
            tf.textAlignment = .left
        }
        tf.keyboardType = .numberPad
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let in_email: LeftPaddedTextField = {
        let tf = LeftPaddedTextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_email", comment: "")
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
            tf.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
            tf.textAlignment = .left
        }
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
 
    let birthDateButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_birthDate", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.textAlignment = .center
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize + 3)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize + 3)
        }
        btn.titleLabel?.adjustsFontForContentSizeCategory = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(birthDateAction), for: .touchUpInside)
        return btn
    }()
    
    @objc fileprivate func birthDateAction(){
        setView(view: darkView, hidden: false)
        setView(view: datePickerContainer, hidden: false)
        setView(view: datePicker, hidden: false)
        setView(view: confirmButton, hidden: false)
        setView(view: cancelButton, hidden: false)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    lazy var darkView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        view.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissDarkView))
        view.addGestureRecognizer(tap)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    @objc func dismissDarkView(){
        setView(view: darkView, hidden: true)
        setView(view: datePickerContainer, hidden: true)
        setView(view: datePicker, hidden: true)
        setView(view: confirmButton, hidden: true)
        setView(view: cancelButton, hidden: true)
    }
    
    let datePickerContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let datePicker : UIDatePicker = {
        let dp = UIDatePicker()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            dp.timeZone = TimeZone(identifier: "IRST")
            dp.calendar = Calendar(identifier: .persian)
            dp.locale = Locale(identifier: "fa_IR")
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            dp.timeZone = TimeZone(identifier: "EST")
            dp.locale = Locale(identifier: "en")
        }
        dp.datePickerMode = .date
        dp.backgroundColor = .white
        dp.isHidden = false
        dp.translatesAutoresizingMaskIntoConstraints = false
        return dp
    }()
    
    let confirmButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "datePickerConfirmButton", comment: ""), for: .normal)
        btn.backgroundColor = UIColor(named: "blue")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: 18)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        }
        btn.isHidden = true
        btn.addTarget(self, action: #selector(confirmBtnAct), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func confirmBtnAct(){
        let date = datePicker.date
        let formatter = DateFormatter()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            formatter.calendar = Calendar(identifier: .persian)
            formatter.dateFormat = "yyyy/MM/dd"
            dateText = formatter.string(from: date)
            setView(view: darkView, hidden: true)
            birthDateButton.setTitle(formatter.string(from: date), for: .normal)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            formatter.dateFormat = "yyyy/MM/dd"
            dateText = formatter.string(from: date)
            setView(view: darkView, hidden: true)
            birthDateButton.setTitle(formatter.string(from: date), for: .normal)
        }
    }
    
    let cancelButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "datePickerCancelButton", comment: ""), for: .normal)
        btn.backgroundColor = UIColor(named: "red")
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: 18)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: 18)
        }
        btn.isHidden = true
        btn.addTarget(self, action: #selector(dismissDarkView), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate func setupDatePickerLayout(){
        view.addSubview(darkView)
        darkView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        darkView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        darkView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        darkView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        darkView.addSubview(datePickerContainer)
        datePickerContainer.centerYAnchor.constraint(equalTo: darkView.centerYAnchor,constant: -24).isActive = true
        datePickerContainer.leadingAnchor.constraint(equalTo: darkView.leadingAnchor, constant: 12).isActive = true
        datePickerContainer.trailingAnchor.constraint(equalTo: darkView.trailingAnchor, constant: -12).isActive = true
        datePickerContainer.heightAnchor.constraint(equalTo: darkView.heightAnchor, multiplier: 0.5).isActive = true
        
        datePickerContainer.addSubview(datePicker)
        datePicker.topAnchor.constraint(equalTo: datePickerContainer.topAnchor).isActive = true
        datePicker.leadingAnchor.constraint(equalTo: datePickerContainer.leadingAnchor).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: datePickerContainer.trailingAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalTo: datePickerContainer.heightAnchor, multiplier: 0.85).isActive = true
        
        datePickerContainer.addSubview(confirmButton)
        confirmButton.topAnchor.constraint(equalTo: datePicker.bottomAnchor).isActive = true
        confirmButton.trailingAnchor.constraint(equalTo: datePickerContainer.trailingAnchor).isActive = true
        confirmButton.bottomAnchor.constraint(equalTo: datePickerContainer.bottomAnchor).isActive = true
        confirmButton.widthAnchor.constraint(equalTo: datePickerContainer.widthAnchor, multiplier: 0.5).isActive = true
        
        datePickerContainer.addSubview(cancelButton)
        cancelButton.topAnchor.constraint(equalTo: datePicker.bottomAnchor).isActive = true
        cancelButton.leadingAnchor.constraint(equalTo: datePickerContainer.leadingAnchor).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: datePickerContainer.bottomAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: datePickerContainer.widthAnchor, multiplier: 0.5).isActive = true
    }
    
    fileprivate func setupUserProfileTextViewLayout(){
        view.addSubview(userProfileTextView)
        userProfileTextView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        userProfileTextView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        userProfileTextView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        //userProfileTV.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.05).isActive = true
        userProfileTextView.sizeToFit()
    }
    
    fileprivate func setupContainerViewLayout(){
        view.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: userProfileTextView.bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 12).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 385).isActive = true
    }
    
    fileprivate func setupBackImageBorderLayout(){
        containerView.addSubview(backProfileImage)
        backProfileImage.topAnchor.constraint(equalTo: containerView.topAnchor,constant: 3).isActive = true
        backProfileImage.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -11).isActive = true
        backProfileImage.widthAnchor.constraint(equalToConstant: 92).isActive = true
        backProfileImage.heightAnchor.constraint(equalToConstant: 92).isActive = true
    }
    
    fileprivate func setupImageViewLayout() {
        containerView.addSubview(profileImage)
        profileImage.topAnchor.constraint(equalTo: containerView.topAnchor,constant: 4).isActive = true
        profileImage.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -12).isActive = true
        profileImage.widthAnchor.constraint(equalToConstant: 90).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant: 90).isActive = true
    }
    
    fileprivate func setupBackPencilLayout(){
        containerView.addSubview(backPencil)
        backPencil.topAnchor.constraint(equalTo: profileImage.topAnchor, constant: 50).isActive = true
        backPencil.trailingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10).isActive = true
        backPencil.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backPencil.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backPencil.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceRightToLeft : .forceRightToLeft
    }
    
    fileprivate func setupPencilImageLayout(){
        backPencil.addSubview(pencilImage)
        pencilImage.topAnchor.constraint(equalTo: backPencil.topAnchor, constant: 6).isActive = true
        pencilImage.leadingAnchor.constraint(equalTo: backPencil.leadingAnchor, constant: 6).isActive = true
        pencilImage.trailingAnchor.constraint(equalTo: backPencil.trailingAnchor, constant: -6).isActive = true
        pencilImage.bottomAnchor.constraint(equalTo: backPencil.bottomAnchor, constant: -6).isActive = true
        pencilImage.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceRightToLeft : .forceRightToLeft
    }
    
    fileprivate func setupDescriptionTextViewLayout(){
        containerView.addSubview(descriptionTV)
        descriptionTV.centerYAnchor.constraint(equalTo: profileImage.centerYAnchor).isActive = true
        descriptionTV.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 4).isActive = true
    }
    
    fileprivate func setupFirstNameInLayout(){
        containerView.addSubview(in_firstName)
        let heightSize = UIFont.labelFontSize + 30
        in_firstName.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 16).isActive = true
        in_firstName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 12).isActive = true
        in_firstName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -12).isActive = true
        in_firstName.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        in_firstName.setCornerRadius(size: heightSize/2)
    }
    
    fileprivate func setupLastNameInLayout(){
        containerView.addSubview(in_lastName)
        let heightSize = UIFont.labelFontSize + 30
        in_lastName.topAnchor.constraint(equalTo: in_firstName.bottomAnchor, constant: 8).isActive = true
        in_lastName.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 12).isActive = true
        in_lastName.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -12).isActive = true
        in_lastName.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        in_lastName.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupInternationalCodeInLayout(){
        containerView.addSubview(in_internationalCode)
        let heightSize = UIFont.labelFontSize + 30
        in_internationalCode.topAnchor.constraint(equalTo: in_lastName.bottomAnchor, constant: 8).isActive = true
        in_internationalCode.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 12).isActive = true
        in_internationalCode.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -12).isActive = true
        in_internationalCode.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        in_internationalCode.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupEmailInLayout(){
        containerView.addSubview(in_email)
        let heightSize = UIFont.labelFontSize + 30
        in_email.topAnchor.constraint(equalTo: in_internationalCode.bottomAnchor, constant: 8).isActive = true
        in_email.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 12).isActive = true
        in_email.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -12).isActive = true
        in_email.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        in_email.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupButtonLayout(){
        containerView.addSubview(birthDateButton)
        let heightSize = UIFont.labelFontSize + 30
        birthDateButton.topAnchor.constraint(equalTo: in_email.bottomAnchor, constant: 8).isActive = true
        birthDateButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant: 12).isActive = true
        birthDateButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,constant: -12).isActive = true
        birthDateButton.heightAnchor.constraint(equalToConstant: heightSize).isActive = true

        birthDateButton.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupStackViewLayout(){
        stack = UIStackView(arrangedSubviews: [in_firstName,in_lastName,in_internationalCode,in_email,birthDateButton])
        containerView.addSubview(stack)
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 8.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.backgroundColor = .red
        
        stack.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 12).isActive = true
        stack.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        stack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8).isActive = true
        stack.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "gray")
        setupNavigationBarBackgroundColor()
        setupNavigationView()
        setupUserProfileTextViewLayout()
        setupContainerViewLayout()
        setupBackImageBorderLayout()
        setupImageViewLayout()
        setupBackPencilLayout()
        setupPencilImageLayout()
        setupDescriptionTextViewLayout()
        setupFirstNameInLayout()
        setupLastNameInLayout()
        setupInternationalCodeInLayout()
        setupEmailInLayout()
        setupButtonLayout()
        setupDatePickerLayout()
        //setupStackViewLayout()
        in_firstName.delegate = self
        in_lastName.delegate = self
        in_internationalCode.delegate = self
        in_email.delegate = self
    }
    
    func setupNavigationBarBackgroundColor(){
        if let navigationBar = self.navigationController?.navigationBar {
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [UIColor(named: "blue")?.cgColor as Any, UIColor(named: "green")?.cgColor as Any]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    func setupNavigationView(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "signUp", comment: ""), style: .plain, target: self, action: #selector(SignUp_Action))
        navigationController?.navigationBar.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceLeftToRight
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "registeration", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "IRANSansWeb-Bold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "IRANSansWeb", size: 14.0)!], for: .normal)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS-Bold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font:UIFont(name: "ComicSansMS", size: 14.0)!], for: .normal)
        }
    }
    
    @objc func SignUp_Action() {
        if checkNames() {
            sendAllDataToServer()
        }
    }
    
    func checkNames() -> Bool {
        firstNameText = in_firstName.text
        lastNameText = in_lastName.text
        emailAddress = in_email.text
        internationalCode = in_internationalCode.text
        
        if firstNameText!.isEmpty {
            showError(msg: "First name is empty!")
            return false
        } else if lastNameText!.isEmpty {
            showError(msg: "Last name is empty!")
            return false
        } else if internationalCode!.isEmpty {
            showError(msg: "International code is empty!")
            return false
        } else if (internationalCode.count > 10 || internationalCode.count < 10) {
            showError(msg: "Wrong international code!")
            return false
        } else if ((birthDateButton.titleLabel?.text?.contains(LocalizationSystem.sharedInstance.localizedStringForKey(key: "in_birthDate", comment: "")))!) {
            showError(msg: "BirthDate is empty!")
            return false
        } else if (!emailAddress!.isEmpty) {
            if (!isValidEmail(testStr: emailAddress)) {
                showError(msg: "wrong email Address")
                return false
            }
        } else if emailAddress.isEmpty {
            emailAddress = " "
        }
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func sendAllDataToServer(){
        let url = URL(string: "https://hamrajoo.net/test/v0/insert_user_info.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let empty = ""
        let parameters = "national_code=\(String(describing:internationalCode!))&email=\(String(describing:emailAddress!))&birth_date=\(String(describing:dateText!))&mobile=\(String(describing:phoneNumber!))&first_name=\(String(describing:firstNameText!))&last_name=\(String(describing:lastNameText!))&image_tag=\(String(describing:empty))&image_data=\(String(describing:empty))"
        
        //print(parameters)
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                let returnData = String(data: data!, encoding: .utf8)
                print("return data : \(String(describing: returnData))")
                if (returnData?.contains("1"))! {
                    self.goToMainPage()
                }
            }
            print("response = \(String(describing: response))")
        }
        task.resume()
    }
    
    func showError(msg : String){
        let error = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        self.present(error,animated: true)
    }
    
    func goToMainPage(){
        DispatchQueue.main.async {
            let mainPage = MainPageVC()
            let navCnt = UINavigationController(rootViewController: mainPage)
            navCnt.navigationBar.isTranslucent = false
            self.present(navCnt,animated: true)
        }
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
}

extension SignUpVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension SignUpVC : BackFromChooseImage {
    func didImagePicked(pickedImg: UIImage) {
        //profileImage.image = pickedImg
        
        let mask = UIImageView(image: UIImage(named: "mask"))
        mask.contentMode = .scaleAspectFit
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.heightAnchor.constraint(equalToConstant: 90).isActive = true
        mask.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        profileImage.mask = mask
        imageName = "photo_"
        imageName?.append(self.randomString(length: 20))
        imageName?.append("_")
        imageName?.append(self.randomString(length: 20))
        imageName?.append(".jpg")

        let conv = pickedImg.jpegData(compressionQuality: 0.3)!
        imageData = conv.base64EncodedString()
        //print(imageData!)

        let dataDecoded : Data = Data(base64Encoded: imageData!, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        profileImage.image = decodedimage
            print(decodedimage?.size)
    }
    
    func didBackFromChooseImage() {
        self.darkView.isHidden = true
    }
}

extension String {
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        var st = self;
        if (self.count % 4 <= 2){
            st += String(repeating: "=", count: (self.count % 4))
        }
        guard let data = Data(base64Encoded: st) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    func base64ToImage() -> UIImage? {
        
        if let url = URL(string: self),let data = try? Data(contentsOf: url),let image = UIImage(data: data) {
            return image
        }
        
        return nil
        
    }
}

extension Data {
    mutating func appendString(string: String) {
        append(string.data(using: .utf8)!)
    }
}
