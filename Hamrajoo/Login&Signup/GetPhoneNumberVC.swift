import UIKit
import Firebase

class GetPhoneNumberVC: UIViewController {
    
    var randNum : String!
    var phoneNum : String!
    
    var status : String!
    var msg : String!
    var userId : String = ""
    
    var firstName : String!
    var lastName : String!
    
    let imageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SMS")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let textView : UITextView = {
        let tv = UITextView()
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "getPhoneNumber", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tv.adjustsFontForContentSizeCategory = true
        tv.isEditable = false
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let textField : UITextField = {
        let tf = UITextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNumberPlaceHolder", comment: "")
        tf.textAlignment = .center
        tf.accessibilityLanguage = "en"
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        tf.keyboardType = .numberPad
        tf.updateFocusIfNeeded()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let button : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirmPhoneNumber", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.textAlignment = .center
//        guard let customFont = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        btn.titleLabel?.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: customFont)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        btn.titleLabel?.adjustsFontForContentSizeCategory = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(submitAct), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupImageViewLayout()
        setupTextViewLayout()
        setupTextFieldLayout()
        setupButtonLayout()
        textField.delegate = self
        getDeviceDetail()
    }
    
    fileprivate func getDeviceDetail(){
        let deviceName = UIDevice.current.name
        print(deviceName)
        let iosVersion = UIDevice.current.systemVersion
        print(iosVersion)
        let uuid = UIDevice.current.identifierForVendor
        print(uuid)
        let deviceType = "iOS"
        print(Messaging.messaging().fcmToken)
        
        let url = URL(string: "https://hamrajoo.net/test/v0/check_add_reg_id.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        //let parameters = "mobile=\(String(describing: phoneNum!))&code=\(String(describing: randNum!))"
        let parameters = "device_id=\(uuid!)&device_token=\(String(describing: Messaging.messaging().fcmToken!))&device_name=\(deviceName)&device_type=\(deviceType)&device_version=\(iosVersion)"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            //Let's convert response sent from a server side script to a NSDictionary object:
            print("data : \(String(describing: data))")
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
                    self.status = json!["status"] as? String
                    self.msg = json!["msg"] as? String
                    self.userId = json!["user_id"] as? String ?? "no data"
                    print("json : \(String(describing: json))")
                    
                    let defaults = UserDefaults.standard
                    defaults.set(self.status, forKey: defaultsKeys.statusKey)
                    defaults.set(self.userId, forKey: defaultsKeys.userIdKey)
                    
                    print("status : \(String(describing: self.status!))")
                    print("msg : \(String(describing: self.msg!))")
                    print("user_id : \(String(describing: json!["user_id"] as? Int))")
                    
                } catch {
                    print(error)
                }
            }
            
        }
        task.resume()
    }
    
    fileprivate func setupImageViewLayout() {
        view.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor,constant: view.frame.height/6).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor,multiplier: 0.4).isActive = true
        imageView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor,multiplier: 0.2).isActive = true
    }
    
    fileprivate func setupTextViewLayout() {
        view.addSubview(textView)
        textView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16).isActive = true
        textView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    fileprivate func setupTextFieldLayout(){
        view.addSubview(textField)
        let heightSize = UIFont.labelFontSize + 30
        textField.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 16).isActive = true
        textField.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 16).isActive = true
        textField.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -16).isActive = true
        textField.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        textField.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupButtonLayout(){
        view.addSubview(button)
        let heightSize = UIFont.labelFontSize + 30
        button.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 16).isActive = true
        button.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: view.frame.width/6).isActive = true
        button.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -view.frame.width/5).isActive = true
        button.heightAnchor.constraint(equalToConstant: heightSize).isActive = true

        button.setCornerRadius(size: heightSize / 2)
    }
    
    func genRandomNumber() -> String{
        let rand = Int.random(in: 1000..<10000)
        print(rand)
        return String(rand)
    }
    
    func convertPersianNumToEnNum(num: String) -> String{
        let numberStr: String = num
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        let final = formatter.number(from: numberStr)
        return final!.stringValue
    }

    @objc func submitAct(sender : UIButton) {
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            if textField.text != nil {
                var numbers = convertPersianNumToEnNum(num: textField.text!)
                numbers = "0" + numbers
                if validatePhoneNumber(value: numbers) {
                    randNum = genRandomNumber()
                    phoneNum = textField.text
                    print(phoneNum!)
                    sendData()
                    //goToVerificationPage()
                } else {
                    showError()
                }
            } else  {
                print("nil nil")
            }
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            if validatePhoneNumber(value: textField.text!) {
                randNum = genRandomNumber()
                phoneNum = textField.text
                print(phoneNum!)
                sendData()
                //goToVerificationPage()
            } else {
                showError()
            }
        }
    }
    
    func showError() {
        let error = UIAlertController(title: "Error", message: "This phone number isn't correct", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        self.present(error, animated: true)
    }
    
    func sendData(){
        let url = URL(string: "https://hamrajoo.net/test/v0/send_sms.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters = "mobile=\(String(describing: phoneNum!))&code=\(String(describing: randNum!))"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            //Let's convert response sent from a server side script to a NSDictionary object:
            print("data : \(String(describing: data))")
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
                    self.firstName = json!["first_name"] as? String
                    self.lastName = json!["last_name"] as? String
                    print("json : \(String(describing: json))")
                    self.goToVerificationPage()
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    func validatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(0))[0-9]{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func goToVerificationPage(){
        DispatchQueue.main.async {
            let verification = verificationVC()
            verification.validationNumber = self.randNum
            verification.phoneNumber = self.phoneNum
            verification.firstName = self.firstName
            verification.lastName = self.lastName
            let defaults = UserDefaults.standard
            let stringTwo = defaults.string(forKey: defaultsKeys.userIdKey)
            print(stringTwo)
            print(self.userId)
            if stringTwo == nil {
                verification.userId = self.userId
            } else {
                verification.userId = stringTwo ?? "no data data"
            }
            verification.modalTransitionStyle = .flipHorizontal
            self.present(verification, animated: true)
        }
    }

}

extension GetPhoneNumberVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.count
        if length > 11 {
            return false
        }
        return true
    }
}
