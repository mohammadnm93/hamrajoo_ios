import UIKit

protocol BackFromChooseImage {
    func didBackFromChooseImage()
    func didImagePicked(pickedImg : UIImage)
}

class SelectProfilePictureVC: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    var delegate : BackFromChooseImage!
    var cameraDevice : UIImagePickerController.CameraDevice = .rear

    override func viewDidLoad() {
        super.viewDidLoad()
        setupContainerView()
        setupTitleView()
        setupCameraView()
        setupCameraTextView()
        setupGalleryView()
        setupGalleryTextView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    
    @objc fileprivate func dismissView(){
        self.dismiss(animated: true, completion: nil)
        delegate.didBackFromChooseImage()
    }
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleView : UITextView = {
        let tv = UITextView()
        tv.isSelectable = false
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "choosePhotoTitle", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: 22)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: 22)
        }
        tv.textColor = .black
        tv.sizeToFit()
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    lazy var cameraView : UIImageView = {
        let iv = UIImageView(image: UIImage(named: "camera"))
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(openCamera(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var cameraTextView : UITextView = {
        let tv = UITextView()
        tv.isSelectable = false
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "chooseByCamera", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: 18)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: 20)
            tv.textAlignment = .left
        }
        tv.textColor = UIColor(named: "darkBlue")
        tv.sizeToFit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(openCamera(tapGestureRecognizer:)))
        tv.addGestureRecognizer(tap)
        tv.isUserInteractionEnabled = true
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    @objc fileprivate func openCamera(tapGestureRecognizer: UITapGestureRecognizer){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("chi chi migi")
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.cameraCaptureMode = .photo
            imagePicker.cameraDevice = self.cameraDevice
            imagePicker.cameraFlashMode = .auto
            imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
        }
    }
    
    lazy var galleryView : UIImageView = {
        let iv = UIImageView(image: UIImage(named: "gallery"))
        iv.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(openGallery(tapGestureRecognizer:)))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var galleryTextView : UITextView = {
        let tv = UITextView()
        tv.isSelectable = false
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "chooseByGallery", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: 18)
            tv.textAlignment = .right
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: 20)
            tv.textAlignment = .left
        }
        tv.textColor = UIColor(named: "darkBlue")
        tv.sizeToFit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(openGallery(tapGestureRecognizer:)))
        tv.addGestureRecognizer(tap)
        tv.isUserInteractionEnabled = true
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
//    func checkPermission() {
//        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
//        switch photoAuthorizationStatus {
//        case .authorized:
//            print("Access is granted by user")
//        case .notDetermined:
//            PHPhotoLibrary.requestAuthorization({
//                (newStatus) in
//                print("status is \(newStatus)")
//                if newStatus ==  PHAuthorizationStatus.authorized {
//                    /* do stuff here */
//                    print("success")
//                }
//            })
//            print("It is not determined until now")
//        case .restricted:
//            // same same
//            print("User do not have access to photo album.")
//        case .denied:
//            // same same
//            print("User has denied the permission.")
//        }
//    }
    
    @objc fileprivate func openGallery(tapGestureRecognizer: UITapGestureRecognizer){
        print("hello")
        //checkPermission()
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage: UIImage
        
        if let possibleImage = info[.editedImage] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info[.originalImage] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }
        
        delegate.didImagePicked(pickedImg: newImage)
        dismiss(animated: true)
        dismissView()
    }
    
    fileprivate func setupContainerView(){
        view.addSubview(containerView)
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -4).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 170).isActive = true
    }
    
    fileprivate func setupTitleView(){
        containerView.addSubview(titleView)
        titleView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        titleView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
    }
    
    fileprivate func setupCameraView(){
        containerView.addSubview(cameraView)
        cameraView.topAnchor.constraint(equalTo: titleView.bottomAnchor).isActive = true
        cameraView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16).isActive = true
        cameraView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        cameraView.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    fileprivate func setupCameraTextView(){
        containerView.addSubview(cameraTextView)
        cameraTextView.leadingAnchor.constraint(equalTo: cameraView.trailingAnchor, constant: 16).isActive = true
        cameraTextView.topAnchor.constraint(equalTo: titleView.bottomAnchor).isActive = true
        cameraTextView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
    }
    
    fileprivate func setupGalleryView(){
        containerView.addSubview(galleryView)
        galleryView.topAnchor.constraint(equalTo: cameraView.bottomAnchor, constant: 8).isActive = true
        galleryView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16).isActive = true
        galleryView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        galleryView.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    fileprivate func setupGalleryTextView(){
        containerView.addSubview(galleryTextView)
        galleryTextView.leadingAnchor.constraint(equalTo: galleryView.trailingAnchor, constant: 16).isActive = true
        galleryTextView.topAnchor.constraint(equalTo: cameraTextView.bottomAnchor, constant: 4).isActive = true
        galleryTextView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
    }
    
}
