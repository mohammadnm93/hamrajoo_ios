import UIKit

class verificationVC : UIViewController {
    
    var count = 120
    var timer = Timer()
    var validationNumber : String!
    var phoneNumber : String!
    var registerState : String!
    
    var firstName : String = ""
    var lastName  : String = ""
    
    var userId : String = ""
    
    let image : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SMS")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var text : UITextView = {
        let tv = UITextView()
        if self.firstName.isEmpty {
           tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verificationNumberTV", comment: "")
        } else {
            if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
                tv.text = "خوش آمدید \(self.firstName) \(self.lastName).لطفا کد تایید را وارد کنید"
            } else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                tv.text = "Welcome \(self.firstName) \(self.lastName).Please enter the validation code"
            }
        }
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tv.adjustsFontForContentSizeCategory = true
        tv.isEditable = false
        tv.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        tv.textAlignment = .center
        tv.sizeToFit()
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let textF : UITextField = {
        let tf = UITextField()
        tf.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verificationCodePlaceHolder", comment: "")
        tf.textAlignment = .center
        tf.accessibilityLanguage = "en"
        tf.keyboardType = .numberPad
        tf.backgroundColor = UIColor(named: "gray")
        tf.textColor = .black
        tf.updateFocusIfNeeded()
        tf.becomeFirstResponder()
        
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tf.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tf.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tf.adjustsFontForContentSizeCategory = true
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let timeTextView : UITextView = {
        let tv = UITextView()
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "time4Verify", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tv.adjustsFontForContentSizeCategory = true
        tv.isEditable = false
        tv.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let submit : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "green")
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "submit4Verify", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.textAlignment = .center
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        btn.titleLabel?.adjustsFontForContentSizeCategory = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeTextView.text = String(LocalizationSystem.sharedInstance.localizedStringForKey(key: "time4Verify", comment: ""))
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.update)), userInfo: nil, repeats: true)
        
        view.backgroundColor = .white
        setupImageViewLayout()
        setupTextViewLayout()
        setupTextFieldLayout()
        setupTimeTextViewLayout()
        setupButtonLayout()
        textF.delegate = self
    }
    
    fileprivate func setupImageViewLayout() {
        view.addSubview(image)
        image.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        image.topAnchor.constraint(equalTo: view.topAnchor,constant: view.frame.height/6).isActive = true
        image.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor,multiplier: 0.4).isActive = true
        image.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor,multiplier: 0.2).isActive = true
    }
    
    fileprivate func setupTextViewLayout() {
        view.addSubview(text)
        text.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 8).isActive = true
        text.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        text.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        text.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    fileprivate func setupTextFieldLayout(){
        view.addSubview(textF)
        let heightSize = UIFont.labelFontSize + 30
        textF.topAnchor.constraint(equalTo: text.bottomAnchor, constant: 16).isActive = true
        textF.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 16).isActive = true
        textF.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -16).isActive = true
        textF.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        
        textF.setCornerRadius(size: heightSize / 2)
    }
    
    fileprivate func setupTimeTextViewLayout(){
        view.addSubview(timeTextView)
        let heightSize = UIFont.labelFontSize + 30
        timeTextView.topAnchor.constraint(equalTo: textF.bottomAnchor, constant: 16).isActive = true
        timeTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        timeTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        timeTextView.heightAnchor.constraint(equalToConstant: heightSize).isActive = true
    }
    
    fileprivate func setupButtonLayout(){
        view.addSubview(submit)
        let heightSize = UIFont.labelFontSize + 30
        submit.topAnchor.constraint(equalTo: timeTextView.bottomAnchor, constant: 16).isActive = true
        submit.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: view.frame.width/6).isActive = true
        submit.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -view.frame.width/5).isActive = true
        submit.heightAnchor.constraint(equalToConstant: heightSize).isActive = true

        submit.setCornerRadius(size: heightSize / 2)
    }
    
    @objc func update(){
        if count == 0 {
            submit.backgroundColor = UIColor(named: "red")
            submit.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "resendCode", comment: ""), for: .normal)
        }
        
        if count >= 0 {
            let minutesCalc = count / 60 % 60
            let minutes = convertEngNumToPersianNum(num: String(minutesCalc))
            let secondCalc = count % 60
            let seconds = convertEngNumToPersianNum(num: String(secondCalc))
            count -= 1
            if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
                timeTextView.text = "\(minutes):\(seconds)"
            } else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                timeTextView.text = String(format: "%02d:%02d", minutesCalc, secondCalc)
            }
        }
        
    }
    
    func convertEngNumToPersianNum(num: String)->String{
        let number = NSNumber(value: Int(num)!)
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let faNumber = format.string(from: number)
        return faNumber!
    }
    
    func convertPersianNumToEnNumber(num: String) -> String{
        let numberStr: String = num
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        let final = formatter.number(from: numberStr)
        return final!.stringValue
    }

    @objc func submitAction(sender: UIButton!) {
        if (submit.titleLabel?.text?.contains(LocalizationSystem.sharedInstance.localizedStringForKey(key: "submit4Verify", comment: "")))!{
            if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
                let number = convertPersianNumToEnNumber(num: textF.text!)
                if (number.contains(validationNumber)) {
                    sendVerData()
                    //whichVcShouldShow(result: "0")
                } else {
                    showErr()
                }
            } else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                if (textF.text?.contains(validationNumber))! {
                    sendVerData()
                    //whichVcShouldShow(result: "0")
                } else {
                    showErr()
                }
            }
            
        } else {
            validationNumber = genRandomNumber()
            resendData()
            count = 120
            submit.backgroundColor = UIColor(named: "green")
            submit.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "submit4Verify", comment: ""), for: .normal)
        }
    }
    
    func genRandomNumber() -> String{
        let rand = Int.random(in: 1000..<10000)
        print(rand)
        return String(rand)
    }
    
    func showErr() {
        let error = UIAlertController(title: "Error", message: "This verification number isn't correct", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        error.addAction(ok)
        self.present(error, animated: true)
    }
    
    func sendVerData(){
        let url = URL(string: "https://hamrajoo.net/test/v0/check_insert_mobile_user.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters = "mobile=\(phoneNumber!))&user_id=\(userId)"
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            } else {
                let returnData = String(data: data!, encoding: .utf8)
                print("return data : \(String(describing: returnData))")
                self.whichVcShouldShow(result: returnData!)
            }
            print("response = \(String(describing: response))")
            
        }
        task.resume()
    }
    
    func resendData(){
        let url = URL(string: "https://hamrajoo.net/test/send.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let parameters = "mobile=\(String(describing: phoneNumber!))&code=\(String(describing: validationNumber!))"
        //print(phoneNumber)
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            //Let's convert response sent from a server side script to a NSDictionary object:
            print("data : \(String(describing: data))")
            //            if let data = data {
            //                do {
            //                    let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
            //                    print("json : \(json)")
            //                } catch {
            //                    print(error)
            //                }
            //            }
            
            
        }
        task.resume()
    }
    
    func whichVcShouldShow(result : String){
        DispatchQueue.main.async {
            let res = Int(result)
            switch res {
            case -1 :
                self.goToMainPage()
                break
            case 0 :
                self.goToSignUpVC()
                break
            case 1 :
                self.goToSignUpVC()
                break
            case 2 :
                print("error")
                break
            case .none:
                print("none")
            case .some(_):
                print("something different")
            }
        }
        
    }
    
    func goToSignUpVC(){
        let signUp = SignUpVC()
        let navCnt = UINavigationController(rootViewController: signUp)
        navCnt.navigationBar.isTranslucent = false
        signUp.phoneNumber = self.phoneNumber
        signUp.userID = self.userId
        signUp.modalTransitionStyle = .flipHorizontal
        self.present(navCnt, animated: true)
    }
    
    func goToMainPage(){
        let mainPage = MainPageVC()
        let navCnt = UINavigationController(rootViewController: mainPage)
        navCnt.navigationBar.isTranslucent = false
        self.present(navCnt,animated: true)
    }
}

extension verificationVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.count
        if length > 4 {
            return false
        }
        return true
    }
}

