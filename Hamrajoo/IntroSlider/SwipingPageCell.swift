import Foundation
import UIKit

class SwipingPageCell: UICollectionViewCell {
    
    let topImageContainer : UIView = {
        let cn = UIView()
        cn.backgroundColor = .clear
        cn.translatesAutoresizingMaskIntoConstraints = false
        return cn
    }()
    
    let introImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = .clear
        return iv
    }()
    
    let titleText : UITextView = {
        let tv = UITextView()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: 45)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: 45)
        }
        tv.isEditable = false
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.textColor = .white
        tv.backgroundColor = .clear
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let discText : UITextView = {
        let tv = UITextView()
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: 25)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: 25)
        }
        
        tv.isEditable = false
        tv.textColor = .white
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.backgroundColor = .clear
        tv.textAlignment = .center
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupImageLayout()
        setupTitleTextViewLayout()
        setupDiscTextViewLayout()
        setGradientBackground()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupImageLayout(){
        addSubview(topImageContainer)
        topImageContainer.addSubview(introImage)
        topImageContainer.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive = true
        topImageContainer.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        topImageContainer.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        topImageContainer.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        introImage.centerXAnchor.constraint(equalTo: topImageContainer.centerXAnchor).isActive = true
        introImage.bottomAnchor.constraint(equalTo: topImageContainer.bottomAnchor).isActive = true
        introImage.heightAnchor.constraint(equalTo: topImageContainer.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    fileprivate func setupTitleTextViewLayout(){
        addSubview(titleText)
        titleText.topAnchor.constraint(equalTo: topImageContainer.bottomAnchor).isActive = true
        titleText.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        titleText.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        titleText.heightAnchor.constraint(equalToConstant: 70).isActive = true
    }
    
    fileprivate func setupDiscTextViewLayout(){
        addSubview(discText)
        discText.topAnchor.constraint(equalTo: titleText.bottomAnchor).isActive = true
        discText.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        discText.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        discText.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
}
