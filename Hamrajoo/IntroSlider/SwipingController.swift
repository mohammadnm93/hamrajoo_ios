import Foundation
import UIKit

class SwipingController : UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let imageNames = ["carpooling","airpolution","traffic","income","savemoney"]
    let titleTexts = ["carpooling_title", "airpolution_title", "traffic_title", "income_title", "savemoney_title"]
    let descTexts = ["carpooling_disc", "airpolution_disc", "traffic_disc", "income_disc", "savemoney_disc"]
    
    var stack : UIStackView!
    
    let skipButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "skip_button", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: 16)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: 16)
        }
        return btn
    }()
    
    @objc private func handleSkip(){
        let nextIndex = imageNames.count - 1
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        if pageControl.currentPage == imageNames.count - 1 {
            showNewBottomViews()
        }
    }
    
    let nextButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "next_button", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: 16)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: 16)
        }
        btn.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        return btn
    }()
    
    @objc private func handleNext(){
        let nextIndex = min(pageControl.currentPage + 1, imageNames.count - 1)
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        if pageControl.currentPage == imageNames.count - 1 {
            showNewBottomViews()
        }
    }
    
    let gotitButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "gotit_button", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb", size: 16)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS", size: 16)
        }
        btn.addTarget(self, action: #selector(handleGotit), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleGotit() {
        if CheckInternet.isConnectedToNetwork() {
            print("is connected")
            let viewCont = GetPhoneNumberVC()
            viewCont.modalTransitionStyle = .crossDissolve
            viewCont.modalPresentationStyle = .fullScreen
            present(viewCont, animated: true)
        } else {
            print("isn't connected")
            let showNotConnectedVC = ShowNotConnectedVC()
            showNotConnectedVC.modalTransitionStyle = .crossDissolve
            showNotConnectedVC.modalPresentationStyle = .overCurrentContext
            present(showNotConnectedVC, animated: true)
        }
        
    }
    
    let emptyView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let pageControl : UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.currentPage = 0
        pc.numberOfPages = 5
        pc.backgroundColor = .clear
        pc.currentPageIndicatorTintColor = .white
        pc.pageIndicatorTintColor = UIColor(red: 88/255, green: 135/255, blue: 187/255, alpha: 1)
        return pc
    }()
    
    func showNewBottomViews(){
        if pageControl.currentPage == imageNames.count - 1 {
            nextButton.isHidden = true
            skipButton.isHidden = true
            emptyView.isHidden = false
            gotitButton.isHidden = false
            stack.removeArrangedSubview(skipButton)
            stack.removeArrangedSubview(nextButton)
            stack.removeArrangedSubview(pageControl)
            stack.addArrangedSubview(emptyView)
            stack.addArrangedSubview(pageControl)
            stack.addArrangedSubview(gotitButton)
        }
    }
    
    func showOldBottomViews(){
        if pageControl.currentPage != imageNames.count - 1 {
            nextButton.isHidden = false
            skipButton.isHidden = false
            emptyView.isHidden = true
            gotitButton.isHidden = true
            stack.removeArrangedSubview(emptyView)
            stack.removeArrangedSubview(gotitButton)
            stack.removeArrangedSubview(pageControl)
            stack.addArrangedSubview(skipButton)
            stack.addArrangedSubview(pageControl)
            stack.addArrangedSubview(nextButton)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(SwipingPageCell.self, forCellWithReuseIdentifier: "cellID")
        collectionView.isPagingEnabled = true
        setupBottomStackViewLayout()
    }
    
    fileprivate func setupBottomStackViewLayout(){
        stack = UIStackView(arrangedSubviews: [skipButton,pageControl,nextButton])
        stack.backgroundColor = .gray
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .fillEqually
        view.addSubview(stack)
        
        stack.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.05).isActive = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! SwipingPageCell
        let imageName = imageNames[indexPath.item]
        cell.introImage.image = UIImage(named: imageName)
        cell.titleText.text   = LocalizationSystem.sharedInstance.localizedStringForKey(key: titleTexts[indexPath.item], comment: "")
        cell.discText.text  = LocalizationSystem.sharedInstance.localizedStringForKey(key: descTexts[indexPath.item], comment: "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(x/view.frame.width)
        showOldBottomViews()
        showNewBottomViews()
    }
    
}
