//
//  Protocols.swift
//  Hamrajoo
//
//  Created by Hamrajoo on 8/25/19.
//  Copyright © 2019 Hamrajoo. All rights reserved.
//

import Foundation

protocol HomeControllerDelegate {
    func didHandleMenuToggle()
}

protocol DismissHowMuchYouTalk {
    func didDismissHowMuchYouTalkVC()
    func didRadioGroupSelected(index : Int)
}

protocol DismissAddCarVC {
    func didDismissAddCarViewController()
    func didDataSendBack(carData : [[String : Any]])
}

protocol DismissCarFacilities {
    func didDismissCarFacilitiesViewController()
    func didDataSelected(array : [SwitchStatusModel])
}

protocol CarFacilitiesSwitchDelegate {
    func didTapedSwitch(cell : FacilitiesCell)
}

protocol AddedCarEditDeleteDelegate {
    func didEditCell(cell : AddedCarsCell)
    func didDeleteCell(cell : AddedCarsCell)
}

