import UIKit

class ShowNotConnectedVC: UIViewController {
    
    var topStack : UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        view.addGestureRecognizer(tap)
        setupMainContainerLayouts()
        setupImageContainerLayout()
        setupButtonContainerLayout()
    }
    
    @objc func dismissView(){
        dismiss(animated: true, completion: nil)
    }

    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.setCornerRadius(size: 24)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageContainerView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "red")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageView : UIImageView = {
        let iv = UIImageView(image: UIImage(named: "notConnected"))
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let fTextView : UITextView = {
        let tv = UITextView()
        tv.backgroundColor = .clear
        tv.isScrollEnabled = false
        tv.textColor = .white
        tv.textAlignment = .center
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.labelFontSize)
        }
        tv.isSelectable = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_topTextView", comment: "")
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let sTextView : UITextView = {
        let tv = UITextView()
        tv.backgroundColor = .clear
        tv.isScrollEnabled = false
        tv.textAlignment = .center
        tv.textColor = .black
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            tv.font = UIFont(name: "IRANSansWeb", size: UIFont.labelFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            tv.font = UIFont(name: "ComicSansMS", size: UIFont.labelFontSize)
        }
        tv.isSelectable = false
        tv.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_bottomTextView", comment: "")
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let tryAgainButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(named: "red")
        btn.setCornerRadius(size: 24)
        btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_button", comment: ""), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            btn.titleLabel?.font = UIFont(name: "IRANSansWeb-Bold", size: UIFont.buttonFontSize)
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            btn.titleLabel?.font = UIFont(name: "ComicSansMS-Bold", size: UIFont.buttonFontSize)
        }
        btn.addTarget(self, action: #selector(tryAgainAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func tryAgainAction(){
        if CheckInternet.isConnectedToNetwork() {
            let viewCont = GetPhoneNumberVC()
            viewCont.modalTransitionStyle = .crossDissolve
            viewCont.modalPresentationStyle = .fullScreen
            present(viewCont, animated: true)
        } else {
            showError()
        }
    }
    
    func showError() {
        let error = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_error_ok", comment: ""), style: .cancel) { (_) in
            error.dismiss(animated: true, completion: nil)
        }
        
        var titleFont = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS-Bold", size: 18.0)!]
        var messageFont = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS-Bold", size: 12.0)!]
        
        if LocalizationSystem.sharedInstance.getLanguage() == "fa" {
            
            titleFont = [NSAttributedString.Key.font: UIFont(name: "IRANSansWeb-Bold", size: 18.0)!]
            messageFont = [NSAttributedString.Key.font: UIFont(name: "IRANSansWeb", size: 12.0)!]
        } else {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            
            titleFont = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS-Bold", size: 18.0)!]
            messageFont = [NSAttributedString.Key.font: UIFont(name: "ComicSansMS", size: 12.0)!]
        }
        
        let titleAttrString = NSMutableAttributedString(string: LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_error_title", comment: ""), attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: LocalizationSystem.sharedInstance.localizedStringForKey(key: "showNotConnected_error_msg", comment: ""), attributes: messageFont)
        
        error.setValue(titleAttrString, forKey: "attributedTitle")
        error.setValue(messageAttrString, forKey: "attributedMessage")
        
        error.addAction(ok)
        present(error,animated: true)
    }
    
    fileprivate func setupMainContainerLayouts(){
        view.addSubview(containerView)
        containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor,constant: -24).isActive = true
        containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 12).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -12).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor,multiplier: 0.5).isActive = true
    }
    
    fileprivate func setupImageContainerLayout(){
        containerView.addSubview(imageContainerView)
        imageContainerView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        imageContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        imageContainerView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        imageContainerView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5).isActive = true
        
        topStack = UIStackView(arrangedSubviews: [imageView,fTextView])
        imageContainerView.addSubview(topStack)
        topStack.axis = .vertical
        topStack.alignment = .center
        topStack.backgroundColor = .clear
        topStack.translatesAutoresizingMaskIntoConstraints = false
        
        topStack.topAnchor.constraint(equalTo: imageContainerView.topAnchor).isActive = true
        topStack.leadingAnchor.constraint(equalTo: imageContainerView.leadingAnchor).isActive = true
        topStack.trailingAnchor.constraint(equalTo: imageContainerView.trailingAnchor).isActive = true
        topStack.bottomAnchor.constraint(equalTo: imageContainerView.bottomAnchor).isActive = true
    }
    
    fileprivate func setupButtonContainerLayout() {
        containerView.addSubview(sTextView)
        sTextView.topAnchor.constraint(equalTo: imageContainerView.bottomAnchor, constant: 24).isActive = true
        sTextView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        sTextView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        sTextView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.1).isActive = true
        
        containerView.addSubview(tryAgainButton)
        tryAgainButton.topAnchor.constraint(equalTo: sTextView.bottomAnchor, constant: 24).isActive = true
        tryAgainButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        tryAgainButton.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.5).isActive = true
        tryAgainButton.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.15).isActive = true
    }
    
}
